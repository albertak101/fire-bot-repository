package com.albert.finalYearProject.security.jwt;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.albert.finalYearProject.payload.UserProfile;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder

public class JwtUserDetail implements UserDetails {

	/**
	 * 
	 */
	   private static final long serialVersionUID = 1L;
	
	
	    private Long id;

	    private String name;

	    private String username;
	    
	    private String companyAccount;
	    
	    private String userAccountIdentity;

	    @JsonIgnore
	    private String email;

	    @JsonIgnore
	    private String password;

	    private Collection<? extends GrantedAuthority> authorities;

	    @Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		
		return authorities;
	}

	@Override
	public String getPassword() {
		
		return password;
	}

	@Override
	public String getUsername() {
		
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
	
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		
		return true;
	}

	@Override
	public boolean isEnabled() {
		
		return true;
	}
	
	public static JwtUserDetail create(UserProfile user) {
       
//		List<GrantedAuthority> authorities = user.getRoles().stream().map(role ->
//                new SimpleGrantedAuthority(role.getName().name())
//        ).collect(Collectors.toList());

        return JwtUserDetail.builder()
        		             .email(user.getEmail()) 
        		             .id(user.getId())
        		             .name(user.getFirstname() + " " + user.getSurname() + " , " + user.getMiddlename())
        		             .username(user.getUsername())
        		             .password(user.getPassword())
        		             .companyAccount(user.getCompanyId())
        		             .userAccountIdentity(user.getAccountIdentity()) 
        		             
        		             .build();
    }

}
