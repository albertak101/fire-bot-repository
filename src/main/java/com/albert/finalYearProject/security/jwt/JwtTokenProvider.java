package com.albert.finalYearProject.security.jwt;

import static com.albert.finalYearProject.utils.AppConstants.REFRESH_TOKEN_KEY;
import static com.albert.finalYearProject.utils.AppConstants.USER_EMAIL_ADDRESS_KEY;
import static com.albert.finalYearProject.utils.AppConstants.USER_IDENTITY_KEY;
import static com.albert.finalYearProject.utils.AppConstants.USER_COMPANY_KEY;
import static com.albert.finalYearProject.utils.AppConstants.USER_NAME_KEY;
import static com.albert.finalYearProject.utils.AppConstants.USER_FULLNAME_KEY;
import static com.albert.finalYearProject.utils.AppConstants.USER_REFRESH_EXPIRATION_KEY;
import static com.albert.finalYearProject.utils.AppConstants.ISSUER;
import static com.albert.finalYearProject.utils.AppConstants.SECRET;
import static com.albert.finalYearProject.utils.AppConstants.DEFAULT_EXPIRE_IN_SECONDS;
import static com.albert.finalYearProject.utils.AppConstants.KEY_SIZE;


import java.time.Instant;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator.Builder;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;


@Service
public class JwtTokenProvider {
	
	private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

 //   private final static long DEFAULT_EXPIRE_IN_SECONDS = 15 * 60 * 60;
    
   // private final static long REFRESH_EXPIRE_IN_SECONDS = 3* 60 * 60;
    
    //private final static String ISSUER = "metaapps.metawebtechnologiesgh.com";
    
    //private final static String SECRET = "xQyW6Jc6GrWd6O5DuTzdykWhrQm0u9hX8Ih47ytLAFFHWzy3UtXCAyIIJpSKbar";
    
    //
    
    private Algorithm algorithm = Algorithm.HMAC512(SECRET.getBytes());
    

    
    public String generateJWTAccessToken(String username, String role) {
        long now = new Date().getTime();
        long expireTime = now + (DEFAULT_EXPIRE_IN_SECONDS);
        Date expireDate = new Date(expireTime);

        String jwtToken = JWT.create()
                .withIssuer(ISSUER)
                .withClaim("username", username)
                .withClaim("role", role)
                .withExpiresAt(expireDate)                
                .sign(algorithm);

        return jwtToken;
    }
    
    public String generateJWTAccessToken(UserDetails userDetails) {
        long now = new Date().getTime();
        long expireTime = now + (DEFAULT_EXPIRE_IN_SECONDS);
        Date expireDate = new Date(expireTime);
        JwtUserDetail principal = (JwtUserDetail) userDetails;
        String jwtToken = JWT.create()
                .withIssuer(ISSUER)
                .withClaim(USER_NAME_KEY,principal.getUsername())
                .withClaim(USER_COMPANY_KEY, principal.getCompanyAccount())
                .withClaim(USER_EMAIL_ADDRESS_KEY, principal.getEmail())
                .withClaim(USER_FULLNAME_KEY, principal.getName())
                .withKeyId(RandomStringUtils.randomAlphanumeric(16))
                .withClaim(USER_IDENTITY_KEY, principal.getUserAccountIdentity())
                .withClaim(USER_REFRESH_EXPIRATION_KEY,Instant.now().plusSeconds(DEFAULT_EXPIRE_IN_SECONDS).getEpochSecond())
                .withIssuedAt(new Date())
                .withSubject(ISSUER)
                //.withArrayClaim("role", principal.getAuthorities())
                .withExpiresAt(expireDate)                
                .sign(algorithm);

        return jwtToken;
    }
    
    
    public String generateJWTAccessToken(Authentication authentication) {
        long now = new Date().getTime();
        long expireTime = now + (DEFAULT_EXPIRE_IN_SECONDS);
        Date expireDate = new Date(expireTime);
        
       
        JwtUserDetail principal = (JwtUserDetail) authentication.getPrincipal();;
        String jwtToken = JWT.create()
                .withIssuer(ISSUER)
                .withClaim(USER_NAME_KEY,principal.getUsername())
                .withClaim(USER_COMPANY_KEY, principal.getCompanyAccount())
                .withClaim(USER_EMAIL_ADDRESS_KEY, principal.getEmail())
                .withClaim(USER_FULLNAME_KEY, principal.getName())
                .withKeyId(RandomStringUtils.randomAlphanumeric(16))
                .withClaim(USER_IDENTITY_KEY, principal.getUserAccountIdentity())
                .withClaim(USER_REFRESH_EXPIRATION_KEY,Instant.now().plusSeconds(DEFAULT_EXPIRE_IN_SECONDS).getEpochSecond())
                .withIssuedAt(new Date())
                .withSubject(ISSUER)
                //.withArrayClaim("role", principal.getAuthorities())
                .withExpiresAt(expireDate)                
                .sign(algorithm);

        return jwtToken;
    }

    public boolean verifyJWTToken(String token) {
        try {
              JWTVerifier verifier = JWT.require(algorithm)
                                        .withIssuer(ISSUER)
                                        .acceptExpiresAt(DEFAULT_EXPIRE_IN_SECONDS)
                                        .build();
                          verifier.verify(token);
                          return true;
        } catch (JWTVerificationException ex) {
            return false;
        }
    }

    public String getClaimFromToken(String token, String claimKey) {
        DecodedJWT decodedJWT = JWT.decode(token);
        return decodedJWT.getClaims().get(claimKey).toString();
    }
    
    public String generateRefreshToken() {
        
        return  UUID.randomUUID().toString(); 
        
    }
    
    
    public  Boolean isTokenExpired(String token) {
    	Date expireDate = getTokenExpirationDate(token);
        if (expireDate == null) {
            return true;
        }
        return expireDate.before(new Date());
     }
    
    public Date getTokenExpirationDate(String token) {
        DecodedJWT decodedJwt = getJwtInfo(token);
        if (decodedJwt == null) {
            return null;
        }
        return decodedJwt.getExpiresAt();
    }

    
    private  DecodedJWT getJwtInfo(String token) {
        return JWT.decode(token);
    }
    
    public  Map<String,Claim> getAllClaimsFromToken(String token) {
    	
        DecodedJWT decodedJwt = getJwtInfo(token);
        if (decodedJwt == null) {
            return null;
        }
        return decodedJwt.getClaims();
    }
    
    private  String generateJWTKey() {
    	return RandomStringUtils.randomAlphanumeric(KEY_SIZE);
    }
    
    private  String generateKeyId() {
    	return RandomStringUtils.randomAlphanumeric(KEY_SIZE);
    }
    
    public  String generateRefreshTokenEn() {
    	return RandomStringUtils.randomAlphanumeric(KEY_SIZE);
    }
    
    public  String regenerateAcccessTokenFromClaims(String accessToken) {
    	Map<String, Claim> claims = getAllClaimsFromToken(accessToken);
    	
    	//List<Claim> list = new ArrayList<Claim>(claims.values());
    	Builder builder = JWT.create()
    			             .withKeyId(generateKeyId())
    			             .withJWTId(generateJWTKey())
    			             .withIssuedAt(new Date())
    			             .withExpiresAt(generateTokenExpirationDate())
    			             .withClaim(REFRESH_TOKEN_KEY, generateRefreshTokenEn())
    			             ; 
    	
        for (Map.Entry<String, Claim> entry : claims.entrySet()) {
            builder.withClaim(entry.getKey(), entry.getValue().asString());
        }
 
        return builder.sign(algorithm);
    
    }
    
    public  Date generateTokenExpirationDate() {
        long now = new Date().getTime();
        long expireTime =  now + (DEFAULT_EXPIRE_IN_SECONDS * 1000);
        return new Date(expireTime);
    }
    
    
    public  String getKeyId(String token) {
        DecodedJWT decodedJwt = getJwtInfo(token);
        if (decodedJwt == null) {
            return null;
        }
        return decodedJwt.getKeyId();
    }
    
    public  String getJWTKey(String token) {
        DecodedJWT decodedJwt = getJwtInfo(token);
        if (decodedJwt == null) {
            return null;
        }
        return decodedJwt.getId();
    }
    
    
    public Date getCreatedDateFromToken(String token) {
    	
   	 DecodedJWT decodedJwt = getJwtInfo(token);
        if (decodedJwt == null) {
            return null;
        }
        return decodedJwt.getIssuedAt();    	
   	
   }
   
   public static Long getUserCompanyIdFromToken(String token) {
   	 DecodedJWT decodedJWT = JWT.decode(token);
        return decodedJWT.getClaims().get(USER_COMPANY_KEY).asLong();

   }
   
//   public static Integer getAccountStateFromToken(String token) {
//   	
//   	 DecodedJWT decodedJWT = JWT.decode(token);
//        return decodedJWT.getClaims().get(ACCOUNT_STATE_KEY).asInt();
//   	
//   }
//   
   public  String getUserAccountIdentityFromToken(String token) {
   	
   	return getClaimFromToken(token, USER_IDENTITY_KEY);
   }
   
  public  String getEmailAddressFromToken(String token) {
   	
   	return getClaimFromToken(token,USER_EMAIL_ADDRESS_KEY);
   }
  
  
  public  String getUserNameFromToken(String token) {
	   	
	   	return getClaimFromToken(token,USER_NAME_KEY);
	   }
   
  public String getRefreshToken(String token) {
  	
  	return getClaimFromToken(token,REFRESH_TOKEN_KEY);
  }
    
}
