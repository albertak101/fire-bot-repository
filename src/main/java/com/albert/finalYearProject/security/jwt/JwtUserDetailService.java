package com.albert.finalYearProject.security.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.albert.finalYearProject.entity.User;
import com.albert.finalYearProject.payload.UserProfile;
import com.albert.finalYearProject.repository.UserRepository;

import lombok.extern.java.Log;

@Service
@Log
public class JwtUserDetailService  implements UserDetailsService {

	 @Autowired
	 UserRepository userRepository;
	
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		
		 User user = userRepository.findOneByUsernameOrEmail(username, username)
	                .orElseThrow(() -> 
	                        new UsernameNotFoundException("User not found with username or email : " + username)
	        );
		 
		 
		 
		 return JwtUserDetail.create(UserProfile.builder()
				                                 .accountIdentity(user.getAccountIdentity())
				                                 .activationKey(user.getActivationKey())
				                                 .companyId(user.getCompany().getCompanyKey())
				                                 .dateOfBirth(user.getDateOfBirth())
				                                 .email(user.getEmail())
				                                 .firstname(user.getFirstName())
				                                 .id(user.getId())
				                                 .password(user.getPassword())
				                                 .phone(user.getPhone())
				                                 .username(user.getUsername())
				                                 .build());

	}
	
	@Transactional
    public UserDetails loadUserByAccountIdentity(String identity) {
		
        User user = userRepository.findOneByAccountIdentity(identity).orElseThrow(
            () -> new UsernameNotFoundException("User not found with Identity : " + identity)
        );

		 return JwtUserDetail.create(UserProfile.builder()
                 .accountIdentity(user.getAccountIdentity())
                 .activationKey(user.getActivationKey())
                 .companyId(user.getCompany().getCompanyKey())
                 .dateOfBirth(user.getDateOfBirth())
                 .email(user.getEmail())
                 .firstname(user.getFirstName())
                 .id(user.getId())
                 .password(user.getPassword())
                 .phone(user.getPhone())
                 .username(user.getUsername())
                 .build());
    }

	
}
