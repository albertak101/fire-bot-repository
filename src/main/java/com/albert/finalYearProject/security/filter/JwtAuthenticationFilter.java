package com.albert.finalYearProject.security.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.albert.finalYearProject.security.jwt.JwtTokenProvider;
import com.albert.finalYearProject.security.jwt.JwtUserDetailService;

import lombok.extern.java.Log;


@Service
@Log
public class JwtAuthenticationFilter   extends OncePerRequestFilter {
	
	
	@Autowired
    private JwtTokenProvider tokenProvider;
	
	@Autowired
	private JwtUserDetailService jwtUserDetailService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
	     try {
	            String jwt = getJwtFromRequest(request);

	            if (StringUtils.hasText(jwt) && tokenProvider.verifyJWTToken(jwt)) {
	            	
	              //   String userId = tokenProvider.getUserAccountIdentityFromToken(jwt);
	             //   UserDetails userDetails = jwtUserDetailService.loadUserByAccountIdentity(userId.trim());
	            	
	                String userId = tokenProvider.getUserNameFromToken(jwt);
	                UserDetails userDetails = jwtUserDetailService.loadUserByUsername(userId.trim());
	                
	              ///  log.info("User DetailX: " + userDetails);
	                
	                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
	                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

	                SecurityContextHolder.getContext().setAuthentication(authentication);
	            }
	        } catch (Exception ex) {
	            logger.error("Could not set user authentication in security context", ex);
	        }

	        filterChain.doFilter(request, response);
		
	}
	
	
	private String getJwtFromRequest(HttpServletRequest request) {
		
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return null;
    }


}
