package com.albert.finalYearProject.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Utility class for generating random Strings.
 */
public final class RandomUtil {

    private static final int DEF_COUNT = 64;
    private static final int ACCOUNT_ACTIVATION_DEF_COUNT = 128;

    private RandomUtil() {
    }

    /**
     * Generate a password.
     *
     * @return the generated password
     */
    public static String generatePassword() {
        return RandomStringUtils.randomAlphanumeric(DEF_COUNT);
    }

    /**
     * Generate an activation key.
     *
     * @return the generated activation key
     */
    public static String generateDeviceActivationKey() {
        return RandomStringUtils.randomNumeric(ACCOUNT_ACTIVATION_DEF_COUNT);
    }

    /**
     * Generate a reset key.
     *
     * @return the generated reset key
     */
    public static String generateResetKey() {
    
        return RandomStringUtils.randomNumeric(DEF_COUNT);
    }
    
    public static String generatePasswordSeed() {
        return RandomStringUtils.randomAlphanumeric(DEF_COUNT);
    }
    
    public static String generateAuthorizationId(String id) {
        String currentTime = DateTimeFormatter.ofPattern("yyyyMMddhhmmssSSS").format(LocalDateTime.now());
        return id + currentTime;
    }
    
 
    public static String generate() {
        return RandomStringUtils.random(32, true, true);
    }

    
    public static String generateAccountActivationKey() {
        return RandomStringUtils.random(32, true, true);
    }

    public static String generateClientId() {
        return RandomStringUtils.random(20, true, true);
    }

    public static String generateClientSecret() {
        return RandomStringUtils.random(20, true, true);
    }

    

    public static String generateDeviceId() {
        return RandomStringUtils.random(21, true, true);
    }

    public static String generateDeviceKey() {
        return RandomStringUtils.random(21, true, true);
    }
    
    
    public static String generateCompanyRegistrationKey() {
        return RandomStringUtils.random(32, true, true);
    }

    public static String generateCompanyAccountNumber() {
    	return  RandomStringUtils.randomNumeric(16);
    }
    
    
    public static String generateUserAccountIdentity() {
        return RandomStringUtils.random(128, true, true);
    }
        
        		
}
