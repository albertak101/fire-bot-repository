package com.albert.finalYearProject.utils;

public class AppConstants {
	
	   public static final long EXPIRATION_TIME = 900_000; // 15 mins
	    public static final String TOKEN_PREFIX = "Bearer ";
	    public static final String HEADER_STRING = "Authorization";
	    public static final String SIGN_UP_URL = "/api/services/controller/user";
	    
	    // Expiration time is 15 minutes
	    public final static long DEFAULT_EXPIRE_IN_SECONDS = 15 * 60;
	    
	    public final static long REFRESH_EXPIRE_IN_SECONDS = 60 * 60 * 3;
	    
	    public final static String ISSUER = "firebot.firebotgh.com.gh";
	    
	    public final static String SECRET = "0@sZr,ye!(k;b.8Y^@WYleycg_/r]X=uefe$rO.fh4~=S^z1eK7+2v{(PgvO0@f\r\n";
	    
	   public final static Integer KEY_SIZE = 32;
	    public final static String USER_NAME_KEY = "user_name";
	    public final static String ACCOUNT_STATE_KEY = "state";    

		public static final String USER_EMAIL_ADDRESS_KEY = "email";
		public static final String USER_IDENTITY_KEY = "user_id";
		public static final String USER_FULLNAME_KEY = "full_name";
		public static final String USER_COMPANY_KEY = "company_id";
		public static final String USER_REFRESH_EXPIRATION_KEY = "refresh_key_expires_in";
		
		public static final String REFRESH_TOKEN_KEY = "refresh_token";
		
		public static final String REFRESH_TOKEN_HEADER_KEY = "X-Refresh-Token";
		
		public static final String APPLICATION_ID_HEADER_KEY = "X-Application-Id";
		public static final String APPLICATION_SECRET_HEADER_KEY = "X-Application-Secret";

}
