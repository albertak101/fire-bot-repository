package com.albert.finalYearProject.entity;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder

@Entity
@Table(name = "IOT_USER")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")	
	private Long id;	
	
	@Column(name = "USER_IDENTITY")	
	private String accountIdentity;

	@Column(name = "FIRST_NAME")	
	private String firstName;
	
	@Column(name = "OTHER_NAMES")	
	private String middleName;
	
	@Column(name = "LAST_NAME")	
	private String lastName;
	
	@Column(name = "DATE_OF_BIRTH")	
	private String dateOfBirth;
	
	@Column(name = "EMAIL_ADDRESS")	
	private String email;
	
	@Column(name = "MOBILE_PHONE")	
	private String phone;
	
	@Column(name = "USER_NAME")	
	private String username;
	
	@Column(name = "USER_PASSWORD")	
	private String password;
	
	@Column(name = "ACCOUNT_ACTIVATION_KEY")	
	private String activationKey;
	
	@Column(name = "IS_ACCOUNT_ENABLED")
	private Boolean isAccountEnabled;
	
	@Column(name = "IS_ACCOUNT_ACTIVATED")
	private Boolean isAccountActivated;
	
	@Column(name = "DATETIME_ACCOUNT_CREATED")
	private Instant createdDate;
	
	//@Column(name = "DATETIME_ACCOUNT_ACTIVATED")
	//private Instant userActivatedDate;
	
	@Column(name = "DATETIME_LAST_DISABLED")
	private Instant lastDiabledDateTime;
	
	@Column(name = "DATETIME_LAST_ENABLED")
	private Instant lastEnabledDateTime;
	
	@Column(name = "DATETIME_LAST_ACTIVATED")
	private Instant lastActivatedDateTime;
	
	@Column(name = "LAST_ENABLED_BY")	
	private String lastEnabledBy;
	
	@Column(name = "LAST_DISABLED_BY")	
	private String lastDisabledBy;
	
	@Column(name = "LAST_ACTIVATED_BY")	
	private String LastActivatedBy;
	
	@Column(name = "LAST_DEACTIVATED_BY")	
	private String lastDeactivatedBy;
	
	
	@Column(name = "DATETIME_LAST_DEACTIVATED")
	private Instant deactivatedDate;
	
	@Column(name = "DATETIME_LAST_PASSWORD_RESET")
	private Instant lastPasswordResetDateTime;
	
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "company_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
	private Company company;
	
	
	 
}
