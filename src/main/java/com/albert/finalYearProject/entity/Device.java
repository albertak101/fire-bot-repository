package com.albert.finalYearProject.entity;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder

@Entity
@Table(name = "IOT_DEVICE")
public class Device {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")	
	private Long id;	
	
	@Column(name = "DEVICE_ID")	
	private String deviceId;
	
	@Column(name = "DEVICE_KEY")	
	private String deviceKey;

	@Column(name = "DEVICE_NAME")	
	private String deviceName;
	
	@Column(name = "IS_DEVICE_ENABLED")
	private Boolean isDeviceEnabled;

	@Column(name = "DESCRIPTION")	
	private String description;

	@Column(name = "LATITUDE")	
	private Float latitude;

	@Column(name = "LONGITUDE")	
	private Float longitude;
	
	@Column(name = "IS_DEVICE_ACTIVATED")	
	private Boolean isActivated;
	
	@Column(name = "DATETIME_LAST_ACTIVATED")	
	private Instant dateTimeActivated;
	
	@Column(name = "LAST_ACTIVATED_BY")	
	private String activatedBy;
	
	@Column(name = "DATETIME_LAST_DEACTIVATED")	
	private Instant dateTimeDeactivated;
	
	@Column(name = "DATETIME_REGISTERED")	
	private Instant dateTimeRegistered;
	
	@Column(name = "LAST_DEACTIVATED_BY")	
	private String deactivatedBy;
	
	@Column(name = "DEVICE_ACTIVATION_KEY")	
	private String deviceActivationKey;
	
	@Column(name = "DATETIME_LAST_DISABLED")	
	private Instant dateTimeDisabled;
	
	@Column(name = "LAST_DISABLED_BY")	
	private String disabledBy;
	
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "company_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
	private Company company;
	

}
