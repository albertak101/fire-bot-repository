package com.albert.finalYearProject.entity;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder

@Entity
@Table(name = "IOT_DATA_LOGGER")
public class DataLogger {
	
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "ID")	
private Long id;	

@Column(name = "HUMIDITY_READING")
private Float humidity;

@Column(name = "TEMPERATURE_READING")	
private Float temperature;

@Column(name = "ALTITUDE_READING")	
private Float altitude;

@Column(name = "CO2_READING")	
private Float co2;

@Column(name = "PRESSURE_READING")	
private Float pressure;

@Column(name = "DEVICE_READING_DATETIME")	
private Instant readingTime;

@Column(name = "DATETIME_UPLOADED")
private Instant databaseTime;

@ManyToOne(fetch = FetchType.LAZY, optional = false)
@JoinColumn(name = "device_id", nullable = false)
@OnDelete(action = OnDeleteAction.CASCADE)
private Device device;


}