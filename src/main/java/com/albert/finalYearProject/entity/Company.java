package com.albert.finalYearProject.entity;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder

@Entity
@Table(name = "IOT_COMPANY")
public class Company {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COMPANY_ID")	
	private Long id;	

	@Column(name = "COMPANY_NAME")	
	private String companyName;
	
	@Column(name = "DESCRIPTION")	
	private String comment;	
	
	@Column(name = "COMPANY_ACCOUNT_NUMBER")	
	private String companyAccountNumber;
	
	@Column(name = "COMPANY_REGISTRATION_KEY")	
	private String companyKey;
	
	@Column(name = "CONTACT_EMAIL_ADDRESS")	
	private String contactEmailAddress;
	
	@Column(name = "CONTACT_TELEPHONE_NUMBER")	
	private String contactTelephone;
	
	@Column(name = "CONTACT_PERSON_NAME")	
	private String contactPersonName;
	
	@Column(name = "DATETIME_CREATED")
	private Instant datetimeCreated;
	
	@Column(name = "CREATED_BY")
	private String  createdBy;
	

}
