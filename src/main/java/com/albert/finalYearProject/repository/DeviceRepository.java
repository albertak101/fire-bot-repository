package com.albert.finalYearProject.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.albert.finalYearProject.entity.Device;


@Repository
public interface DeviceRepository extends CrudRepository<Device,Long >{

	List<Device> findByCompanyId(Long companyId);
	Optional<Device> findOneByIdAndCompanyId(Long id, Long companyId);
	Optional <Device> getAllById(Long id);
	
	List<Device> findAllByIsDeviceEnabledTrue();
	List<Device> findAllByIsDeviceEnabledFalse();
	List<Device> findAllByIsActivatedTrue();
	List<Device> findAllByIsActivatedFalse();
	
	List<Device> findAllByCompanyIdAndIsDeviceEnabledTrue(Long companyId);
	List<Device> findAllByCompanyIdAndIsDeviceEnabledFalse(Long companyId);
	List<Device> findAllByCompanyIdAndIsActivatedTrue(Long companyId);
	List<Device> findAllByCompanyIdAndIsActivatedFalse(Long companyId);
	
	Optional<Device> findOneByCompanyIdAndDeviceId(Long companyId, String deviceId);
	Optional<Device> findOneByCompanyIdAndDeviceKey(Long companyId, String deviceKey);

	Optional<Device> findOneByDeviceId(String deviceId);
	Optional <Device> findOneById(Long id);	
	Optional<Device> findOneByDeviceActivationKey(String deviceActivationKey);
	
}
