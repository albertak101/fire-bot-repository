package com.albert.finalYearProject.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.albert.finalYearProject.entity.Company;


@Repository
public interface CompanyRepository  extends CrudRepository<Company,Long > {
	
	Optional<Company> findOneByCompanyAccountNumber(String companyAccountNumber);
	Optional<Company> findOneByCompanyKey(String companyKey);

}
