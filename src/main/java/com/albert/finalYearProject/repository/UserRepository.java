package com.albert.finalYearProject.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.albert.finalYearProject.entity.User;

//@Qualifier("user")
@Repository
public interface UserRepository extends CrudRepository<User,Long >{

	Optional <User> getAllById(Long id);
	List<User> getAllByUsername(String username);
	List<User> getAllByEmail(String email);
	
	Optional<User> findOneByEmail(String string);
	Optional<User> findOneByUsername(String username);
	Optional<User> findOneByUsernameOrEmail(String username, String email);
	Optional<User> findOneByActivationKey(String activationKey);
	Optional<User> findOneByAccountIdentity(String accountIdentity);
	
}
