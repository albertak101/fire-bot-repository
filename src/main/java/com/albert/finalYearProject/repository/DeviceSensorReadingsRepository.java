package com.albert.finalYearProject.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.albert.finalYearProject.entity.DataLogger;


@Repository
public interface DeviceSensorReadingsRepository extends CrudRepository<DataLogger,Long >  {
	
	List<DataLogger> getAllByDeviceId(String deviceId);	
	Optional <DataLogger> getOneById(Integer id);
	
}
	
	

