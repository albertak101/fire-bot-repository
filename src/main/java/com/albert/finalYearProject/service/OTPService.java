package com.albert.finalYearProject.service;

public interface OTPService {
	
	Integer generateOTPKey(String otpKey);
	Boolean validateOTPkey(String optKey);
	Integer getOTPFromKey(String otpKey);
	String getKeyFromOtp(Integer otp);
	void clearOTP(String otpKey);

}
