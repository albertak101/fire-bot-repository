package com.albert.finalYearProject.service;

import java.util.List;
import java.util.Optional;

import com.albert.finalYearProject.entity.User;
import com.albert.finalYearProject.payload.CreateUserRequest;
import com.albert.finalYearProject.payload.CreateUserResponse;
import com.albert.finalYearProject.payload.UserProfile;
import com.albert.finalYearProject.payload.UserUpdateRequest;

public interface UserService {

	CreateUserResponse addNewUser(CreateUserRequest createUserRequest);
	UserUpdateRequest editUser(UserUpdateRequest userUpdateRequest) ;
	List<UserProfile>  getAllUsers();
	Optional<UserProfile> getUserById(Long id);
	List <UserProfile> getUserByEmail(String email);
	List <UserProfile> getUserByUsername(String username);
	Boolean deleteUser(String accountId);
    //User Login(UserLoginRequest userLoginRequest);
	Optional<UserProfile> getUserProfile(String usernameOrEmail);
	Optional<UserProfile> userChangePassword(String usernameOrEmail, String newPasword, String oldPassword );
	String rememberUserPassword(String usernameOrEmail);
	Optional<UserProfile> updateUserProfile(String userNameOrEmail,UserUpdateRequest userUpdateRequest) ;
	
	Optional<UserProfile> approveUserAccount(String emailAddress, String byUserAccount);
	Optional<UserProfile>  activateUserAccount(String activationKey );
	Optional<UserProfile> disableUserAccount(String emailAddress, String byUserAccount);
	Boolean enableUserAccount(Integer id, String byUserAccount);
	Optional<UserProfile> deactivateUserAccount(String emailAddress, String byUserAccount);
	Boolean isUserNameTaken(String userName);
	Boolean isEmailAddressTaken(String emailAddress);
	Optional <UserProfile> getUserByAccountIdentity(String accountId);
	Optional<UserProfile> resetUserAccountPassword(String userNameOrEmail, String newPassword);
	
	
 
}
