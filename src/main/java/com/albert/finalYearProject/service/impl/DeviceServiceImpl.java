package com.albert.finalYearProject.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import com.albert.finalYearProject.entity.Company;
import com.albert.finalYearProject.entity.Device;
import com.albert.finalYearProject.payload.AddDeviceRequest;
import com.albert.finalYearProject.payload.AddDeviceResponse;
import com.albert.finalYearProject.payload.DeviceApi;
import com.albert.finalYearProject.payload.DeviceUpdateRequest;
import com.albert.finalYearProject.repository.CompanyRepository;
import com.albert.finalYearProject.repository.DeviceRepository;
import com.albert.finalYearProject.service.DeviceService;
import com.albert.finalYearProject.utils.RandomUtil;

@Service
public class DeviceServiceImpl implements DeviceService{
	
	@Autowired
	DeviceRepository deviceRepository;
	
	@Autowired
	CompanyRepository companyRepository;


	@Override
	public Optional<AddDeviceResponse> addNewDevice(AddDeviceRequest deviceRequest) {
		
		Optional<Company> company = this.companyRepository.findOneByCompanyKey(deviceRequest.getDeviceOwnerId());
		
		if(! company.isPresent())
		     return Optional.empty();
		
		Device device = Device.builder()
                			  .deviceName(deviceRequest.getDeviceName())
                              .deviceId(RandomUtil.generateDeviceId())
                              .deviceKey(RandomUtil.generateDeviceKey())
                              .company(company.get())
                              .isActivated(false)
                              .dateTimeRegistered(Instant.now())
                              .isDeviceEnabled(false) 
                              .description(deviceRequest.getDescription())
                              .latitude(deviceRequest.getLatitude())
                              .longitude(deviceRequest.getLongitude())
                              .build();
                              
	Device result =	this.deviceRepository.save(device);

    return	Optional.of( AddDeviceResponse .builder()
							.deviceId(result.getDeviceId())
							.deviceName(result.getDeviceName())
							.deviceKey(result.getDeviceKey())						
							.build());
	}
	

	@Override
	public List<DeviceApi> getAllDevices() {
	
		List<Device> devices = Streamable.of(this.deviceRepository
                .findAll()).toList();		                                
		
		return devices
				    .stream()
				    .map(dev -> { return DeviceApi.builder()
				    		            .dateTimeActivated(dev.getDateTimeActivated())
   	        		                    .description(dev.getDescription())
   	        		                    .deviceId(dev.getDeviceId())
   	        		                    .deviceKey(dev.getDeviceKey())
   	        		                    .deviceName(dev.getDeviceName())
   	        		                    .id(dev.getId())
   	        		                    .isActivated(dev.getIsDeviceEnabled())
   	        		                    .latitude(dev.getLatitude())
   	        		                    .longitude(dev.getLongitude())
   	        		                    .companyId(dev.getCompany().getCompanyKey())
   	        		                    .isDeviceEnabled(dev.getIsDeviceEnabled())
				    		            .build();
				                } )
				    .collect(Collectors.toList());
	}

	@Override
	public Optional<DeviceApi> getDeviceById(String deviceId) {
		
		 Optional<Device> dev = this.deviceRepository.findOneByDeviceId(deviceId);
			if( dev.isEmpty())
			   return Optional.empty();
			
			return Optional.ofNullable(DeviceApi.builder()
		            .dateTimeActivated(dev.get().getDateTimeActivated())
	                    .description(dev.get().getDescription())
	                    .deviceId(dev.get().getDeviceId())
	                    .deviceKey(dev.get().getDeviceKey())
	                    .deviceName(dev.get().getDeviceName())
	                    .id(dev.get().getId())
	                    .isActivated(dev.get().getIsDeviceEnabled())
	                    .latitude(dev.get().getLatitude())
	                    .longitude(dev.get().getLongitude())
	                    .companyId(dev.get().getCompany().getCompanyKey())
	                    .isDeviceEnabled(dev.get().getIsDeviceEnabled())
		            .build());
	}

	

	@Override
	public List<Device> getDeviceByOwner(Long companyId) {
	 	
		List<Device> devices = this.deviceRepository
                                         .findByCompanyId(companyId);
				                         
		                                
		
		return devices;
	}




	@Override
	public List<DeviceApi> getAllCompanyDevices(String companyKey) {
	
		Optional<Company> company = this.companyRepository.findOneByCompanyKey(companyKey);
		if( ! company.isPresent())
			return new ArrayList<>();
		
		List<Device> devices = deviceRepository.findByCompanyId(company.get().getId());
		
		return  devices
				      .stream()
				      .map(dev -> {
				    	           return  DeviceApi
				    	        		       .builder()
				    	        		       .dateTimeActivated(dev.getDateTimeActivated())
				    	        		       .description(dev.getDescription())
				    	        		       .deviceId(dev.getDeviceId())
				    	        		       .deviceKey(dev.getDeviceKey())
				    	        		       .deviceName(dev.getDeviceName())
				    	        		       .id(dev.getId())
				    	        		       .isActivated(dev.getIsDeviceEnabled())
				    	        		       .isDeviceEnabled(dev.getIsDeviceEnabled())
				    	        		       .latitude(dev.getLatitude())
				    	        		       .longitude(dev.getLongitude())
				    	        		       .companyId(dev.getCompany().getCompanyKey())
				    	        		       .build();
				               }
				      ).collect(Collectors.toList());
	}




	@Override
	public Optional<DeviceApi> getCompanyDeviceByDeviceId(String companyKey, String deviceId) {
		
		Optional<Company> company = this.companyRepository.findOneByCompanyKey(companyKey);
		if( ! company.isPresent())
		    return Optional.empty();
		
	    Optional<Device> dev =	deviceRepository.findOneByCompanyIdAndDeviceId(company.get().getId(), deviceId);
		 if(! dev.isPresent()) {
			 
			 return Optional.empty();
		 }
	    
    		return  Optional.ofNullable( DeviceApi.builder()
 		                                      .dateTimeActivated(dev.get().getDateTimeActivated())
 		                                      .description(dev.get().getDescription())
 		                                      .deviceId(dev.get().getDeviceId())
 		                                      .deviceKey(dev.get().getDeviceKey())
 		                                      .deviceName(dev.get().getDeviceName())
 		                                      .id(dev.get().getId())
 		                                      .isActivated(dev.get().getIsDeviceEnabled())
 		                                      .isDeviceEnabled(dev.get().getIsDeviceEnabled())
 		                                      .latitude(dev.get().getLatitude())
 		                                      .longitude(dev.get().getLongitude())
 		                                      .companyId(dev.get().getCompany().getCompanyKey())
				                              .build());
	              
	             
	}


	@Override
	public Boolean deregisterDevice( String companyId, String deviceId) {
		
		Optional<Company> company = this.companyRepository.findOneByCompanyKey(companyId);
		if( ! company.isPresent())
		    return false;
		
		Optional<Device>  device = this.deviceRepository.findOneByCompanyIdAndDeviceId(company.get().getId(), deviceId);
		if( !device.isPresent())
		  return false;
		
		   device.get().setIsDeviceEnabled(false);
		   device.get().setDateTimeDisabled(Instant.now());
		   device.get().setDisabledBy("Admin");		
		
		this.deviceRepository.save(device.get());
		
		  return true;
	}


	@Override
	public Boolean deactivateDevice( String companyId, String deviceId) {
		
		Optional<Company> company = this.companyRepository.findOneByCompanyKey(companyId);
		if( ! company.isPresent())
		    return false;
		
		Optional<Device>  device = this.deviceRepository.findOneByCompanyIdAndDeviceId(company.get().getId(), deviceId);
		if( !device.isPresent())
		  return false;
		
		   device.get().setIsActivated(false);
		   device.get().setDateTimeDeactivated(Instant.now());
		   device.get().setDeactivatedBy("Admin");
		
		
		this.deviceRepository.save(device.get());
		
		  return true;
	}


	@Override
	public Boolean deleteCompanyDeviceById( String companyId, String deviceId) {
		
		Optional<Company> company = this.companyRepository.findOneByCompanyKey(companyId);
		if( ! company.isPresent())
		    return false;
		
		Optional<Device>  device = this.deviceRepository.findOneByCompanyIdAndDeviceId(company.get().getId(), deviceId);
		if( !device.isPresent())
		  return false;
	    
		this.deviceRepository.delete(device.get());
		return true;
	
	}


	@Override
	public Optional<DeviceApi> activateDevice(String activationKey) {
	
//		Optional<Company> company = this.companyRepository.findOneByCompanyKey(companyId);
//		if( ! company.isPresent())
//		    return Optional.empty();
		
		Optional<Device>  device = this.deviceRepository.findOneByDeviceActivationKey(activationKey);
		if( !device.isPresent())
		  return Optional.empty();
		
		   device.get().setIsActivated(true);
		   device.get().setDateTimeActivated(Instant.now());
		   device.get().setDeviceActivationKey(null);
		   device.get().setActivatedBy("Admin");
		   
		   var dev = this.deviceRepository.save(device.get());
		
	    return 	Optional.ofNullable(DeviceApi.builder()
                 .dateTimeActivated(dev.getDateTimeActivated())
                 .description(dev.getDescription())
                 .deviceId(dev.getDeviceId())
                 .deviceKey(dev.getDeviceKey())
                 .deviceName(dev.getDeviceName())
                 .id(dev.getId())
                 .isActivated(dev.getIsDeviceEnabled())
                 .isDeviceEnabled(dev.getIsDeviceEnabled())
                 .latitude(dev.getLatitude())
                 .longitude(dev.getLongitude())
                 .companyId(dev.getCompany().getCompanyKey())
	    		                  .build());
		
		
	}


	@Override
	public Optional<DeviceApi> updateCompanyDevice(String companyId, String deviceId, DeviceUpdateRequest deviceUpdateRequest) {
		
		Optional<Company> company = this.companyRepository.findOneByCompanyKey(companyId);
		if( ! company.isPresent())
		    return Optional.empty();
		
		Optional<Device>  device = this.deviceRepository.findOneByCompanyIdAndDeviceId(company.get().getId(), deviceId);
		if( !device.isPresent())
		  return Optional.empty();
		
		   device.get().setDescription(deviceUpdateRequest.getDescription());
		   device.get().setDeviceName(deviceUpdateRequest.getDeviceName());
		   device.get().setLatitude(deviceUpdateRequest.getLatitude());
		   device.get().setLongitude(deviceUpdateRequest.getLongitude());;
		   
		   var dev = this.deviceRepository.save(device.get());
		
	    return 	Optional.ofNullable(DeviceApi.builder()
                 .dateTimeActivated(dev.getDateTimeActivated())
                 .description(dev.getDescription())
                 .deviceId(dev.getDeviceId())
                 .deviceKey(dev.getDeviceKey())
                 .deviceName(dev.getDeviceName())
                 .id(dev.getId())
                 .isActivated(dev.getIsDeviceEnabled())
                 .isDeviceEnabled(dev.getIsDeviceEnabled())
                 .latitude(dev.getLatitude())
                 .longitude(dev.getLongitude())
                 .companyId(dev.getCompany().getCompanyKey())
	    		                  .build());
	}


	@Override
	public Optional<DeviceApi> approveDeviceRegistration(String deviceId) {

      Optional<Device> device = this.deviceRepository.findOneByDeviceId(deviceId);
      
      if(! device.isPresent()) {
    	  
    	  return Optional.empty();
      }
      
       device.get().setIsDeviceEnabled(true);
       device.get().setDeviceActivationKey(RandomUtil.generateDeviceActivationKey());
       
       Device dev = this.deviceRepository.save(device.get());
      
		
	    return 	Optional.ofNullable(DeviceApi.builder()
                .dateTimeActivated(dev.getDateTimeActivated())
                .description(dev.getDescription())
                .deviceId(dev.getDeviceId())
                .deviceKey(dev.getDeviceKey())
                .deviceName(dev.getDeviceName())
                .id(dev.getId())
                .isActivated(dev.getIsDeviceEnabled())
                .isDeviceEnabled(dev.getIsDeviceEnabled())
                .activationKey(dev.getDeviceActivationKey())
                .latitude(dev.getLatitude())
                .longitude(dev.getLongitude())
                .companyId(dev.getCompany().getCompanyKey())
	    		                  .build());
	}


	@Override
	public Optional<DeviceApi> disableDevice(String deviceId) {

	      Optional<Device> device = this.deviceRepository.findOneByDeviceId(deviceId);
	      
	      if(! device.isPresent()) {
	    	  
	    	  return Optional.empty();
	      }
	      
	       device.get().setIsDeviceEnabled(false);
	       device.get().setDeviceActivationKey(null);
	       device.get().setDateTimeDisabled(Instant.now());
	       device.get().setDisabledBy("Admin");
	       
	       Device dev = this.deviceRepository.save(device.get());
	      
			
		    return 	Optional.ofNullable(DeviceApi.builder()
	                .dateTimeActivated(dev.getDateTimeActivated())
	                .description(dev.getDescription())
	                .deviceId(dev.getDeviceId())
	                .deviceKey(dev.getDeviceKey())
	                .deviceName(dev.getDeviceName())
	                .id(dev.getId())
	                .isActivated(dev.getIsDeviceEnabled())
	                .isDeviceEnabled(dev.getIsDeviceEnabled())
	                .latitude(dev.getLatitude())
	                .longitude(dev.getLongitude())
	                .companyId(dev.getCompany().getCompanyKey())
		    		                  .build());
	}


//	@Override
//	public Boolean deleteCompanyDeviceByDeviceKey(Long companyId, String deviceId) {
//		
//	Optional<Device> result = deviceRepository.findOneByCompanyIdAndDeviceId(companyId, deviceId);
//	
//	if (result.isPresent()) {
//		
//		this.deviceRepository.delete(result.get());
//		return true;
//	}
//	
//  	return false;
//	}
	
	
}
