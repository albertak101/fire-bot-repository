package com.albert.finalYearProject.service.impl;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.albert.finalYearProject.entity.Company;
import com.albert.finalYearProject.entity.User;
import com.albert.finalYearProject.payload.CreateUserRequest;
import com.albert.finalYearProject.payload.CreateUserResponse;
import com.albert.finalYearProject.payload.UserProfile;
import com.albert.finalYearProject.payload.UserUpdateRequest;
import com.albert.finalYearProject.repository.UserRepository;
import com.albert.finalYearProject.service.OTPService;
import com.albert.finalYearProject.service.UserService;
import com.albert.finalYearProject.utils.RandomUtil;


@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	OTPService otp;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Override
	public CreateUserResponse addNewUser(CreateUserRequest createUserRequest) {
		
		
		
		
		User user = User.builder()
  			  			.firstName(createUserRequest.getFirstname())
  			  			.middleName(createUserRequest.getMiddlename())
  			  			.lastName(createUserRequest.getSurname())
  			  			.dateOfBirth(createUserRequest.getDateOfBirth())
  			  			.email(createUserRequest.getEmail())
  			  			.phone(createUserRequest.getPhone())
  			  			.username(createUserRequest.getUsername())
  			  			.password(createUserRequest.getPassword()) 
  			  			.accountIdentity(RandomUtil.generateUserAccountIdentity())
  			  			.createdDate(Instant.now())  			  			
  			  			.isAccountActivated(false)
  			  			.isAccountEnabled(false)
  			  		    .password(passwordEncoder.encode(createUserRequest.getPassword())) 
  			  		    .company(Company.builder()
  			  		    		         .companyAccountNumber("1111111111")
  			  		    		         .companyKey("111111111")
  			  		    		         .companyName("Test Company")
  			  		    		         .id(1L)
  			  		    		         .build())
                        .build();
                
User result =	this.userRepository.save(user);

return	CreateUserResponse .builder()
				
				.firstName(result.getFirstName())
				.lastName(result.getLastName())				
				.emailAddress(result.getEmail())				
				.userName(result.getUsername())
				.otherNames(result.getMiddleName())
				.dateOfBirth(result.getDateOfBirth())				
				.phone(result.getPhone())
				
				.build();
	}

	@Override
	public UserUpdateRequest editUser(UserUpdateRequest userUpdateRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserProfile> getAllUsers() {
		List<User> users = Streamable.of(this.userRepository
                            .findAll()).toList();
		
		List<UserProfile> userProfiles = users.stream()
				                                 .map( user -> { 
				                                	 					UserProfile response = UserProfile.builder()
				                         				                       .accountIdentity(user.getAccountIdentity())
				                        				                       .activationKey(user.getActivationKey())
				                        				                       .dateOfBirth(user.getDateOfBirth())
				                        				                       .email(user.getEmail())
				                        				                       .firstname(user.getFirstName())
				                        				                       .middlename(user.getMiddleName())
				                        				                       .phone(user.getPhone())
				                        				                       .surname(user.getLastName())
				                        				                       .username(user.getUsername())
				                        				                       .activationKey(user.getActivationKey())
				                        				                       .companyId(user.getCompany().getCompanyKey())
				                        				                       .dateOfBirth(user.getDateOfBirth())
				                        				                       .id(user.getId())
				                                			                    .build();
				                                	   return response;
				                                 }).collect(Collectors.toList());
				                                
		
		return userProfiles;
	}

	@Override
	public Optional<UserProfile> getUserById(Long id) {
		
		Optional<User> result = this.userRepository.getAllById(id);
		if (result.isPresent())
			  return Optional.empty();
		
		return Optional.of(UserProfile.builder()
				                       .accountIdentity(result.get().getAccountIdentity())
				                       .activationKey(result.get().getActivationKey())
				                       .dateOfBirth(result.get().getDateOfBirth())
				                       .email(result.get().getEmail())
				                       .firstname(result.get().getFirstName())
				                       .middlename(result.get().getMiddleName())
				                       .phone(result.get().getPhone())
				                       .surname(result.get().getLastName())
				                       .username(result.get().getUsername())
				                       .id(result.get().getId())
				                       .activationKey(result.get().getActivationKey())
				                       .companyId(result.get().getCompany().getCompanyKey())
				                       .build());
	}

	@Override
	public List<UserProfile> getUserByEmail(String email) {
		List<User> users = Streamable.of(this.userRepository
                .getAllByEmail(email)).toList();
				
		
	           	List<UserProfile> userResponses = users.stream()
				                                 .map( result -> { 
				                                	 					UserProfile response = UserProfile.builder()
				                         				                       .accountIdentity(result.getAccountIdentity())
				                        				                       .activationKey(result.getActivationKey())
				                        				                       .dateOfBirth(result.getDateOfBirth())
				                        				                       .email(result.getEmail())
				                        				                       .firstname(result.getFirstName())
				                        				                       .middlename(result.getMiddleName())
				                        				                       .phone(result.getPhone())
				                        				                       .surname(result.getLastName())
				                        				                       .username(result.getUsername())
				                                			                .build();
				                                	   return response;
				                                 }).collect(Collectors.toList());
				                                
		
		return userResponses;
	}

	@Override
	public List<UserProfile> getUserByUsername(String username) {
		List<User> users = Streamable.of(this.userRepository
                .getAllByUsername(username)).toList();
				
		

       	List<UserProfile> userResponses = users.stream()
		                                 .map( result -> { 
		                                	 					UserProfile response = UserProfile.builder()
		                         				                       .accountIdentity(result.getAccountIdentity())
		                        				                       .activationKey(result.getActivationKey())
		                        				                       .dateOfBirth(result.getDateOfBirth())
		                        				                       .email(result.getEmail())
		                        				                       .firstname(result.getFirstName())
		                        				                       .middlename(result.getMiddleName())
		                        				                       .phone(result.getPhone())
		                        				                       .surname(result.getLastName())
		                        				                       .username(result.getUsername())
		                                			                .build();
		                                	   return response;
		                                 }).collect(Collectors.toList());
		                                

                      return userResponses;
	}

	@Override
	public Boolean deleteUser(String userid) {
		
		Optional <User> user = this.userRepository.findOneByAccountIdentity(userid);
		
		if(!user.isPresent())
	    	return false;
		this.userRepository.delete(user.get());
		return true;
	}

	@Override
	public Optional<UserProfile> getUserProfile(String usernameOrEmail) {
	
		Optional<User> user = this.userRepository.findOneByUsernameOrEmail(usernameOrEmail, usernameOrEmail);
		 if(! user.isPresent())
	     	return Optional.empty();
		 
		 UserProfile profile = UserProfile.builder()
				          .firstname(user.get().getFirstName())
				          .middlename(user.get().getMiddleName())
				          .surname(user.get().getLastName())
				          .phone(user.get().getPhone())				          
				          .dateOfBirth(user.get().getDateOfBirth())
				          .username(user.get().getUsername())
				          .email(user.get().getEmail())
				          .accountIdentity(user.get().getAccountIdentity())
				          .activationKey(user.get().getActivationKey())
				          .id(user.get().getId())
				          .password(user.get().getPassword())
				          .companyId(user.get().getCompany().getCompanyKey())
				          .build();
		 
		return Optional.of(profile);
				    
		 
	}

	@Override
	public Optional<UserProfile> userChangePassword(String usernameOrEmail, String newPasword, String oldPassword) {
		
		Optional<User> user = this.userRepository.findOneByUsernameOrEmail(usernameOrEmail, usernameOrEmail);
		 if(! user.isPresent())
	     	return Optional.empty();
		 String pwd = user.get().getPassword();
		 if(! passwordEncoder.matches(oldPassword, pwd))
		   return Optional.empty();
		 
		 user.get().setLastPasswordResetDateTime(Instant.now());
		 user.get().setPassword( passwordEncoder.encode(oldPassword));
		 
		 var usr = this.userRepository.save(user.get());
		 
		 return Optional.ofNullable(UserProfile.builder()
					.firstname(usr.getFirstName())
					.surname(usr.getLastName())				
					.email(usr.getEmail())				
					.username(usr.getUsername())
					.middlename(usr.getMiddleName())
					.dateOfBirth(usr.getDateOfBirth())				
					.phone(usr.getPhone())
				     .build());
		 
	}

	
	@Override
	public String rememberUserPassword(String usernameOrEmail) {
		
		Optional<User> result = this.userRepository.findOneByUsernameOrEmail(usernameOrEmail, usernameOrEmail);
		if (! result.isPresent())
			return "";
		return otp.generateOTPKey(usernameOrEmail).toString();
	}

	@Override
	public Optional<UserProfile> updateUserProfile(String userNameOrEmail, UserUpdateRequest userUpdateRequest) {

		
		Optional<User> result = this.userRepository.findOneByUsernameOrEmail(userNameOrEmail, userNameOrEmail);
		if (! result.isPresent())
			return Optional.empty();
		
	   User usr =	result.get();
	   
	     usr.setFirstName(userUpdateRequest.getFirstname());		
	     usr.setLastName(userUpdateRequest.getSurname());
	     usr.setMiddleName(userUpdateRequest.getMiddlename());
	     usr.setPhone(userUpdateRequest.getPhone());	     
		         
	     User savedUser =	this.userRepository.save(usr);
	 
		return Optional.of(UserProfile.builder()
				                .dateOfBirth(savedUser.getDateOfBirth())
				                .email(savedUser.getEmail())
				                .firstname(savedUser.getFirstName())
				                .middlename(savedUser.getMiddleName())				             
				                .phone(savedUser.getPhone())
				                .surname(savedUser.getLastName())
				                .username(savedUser.getUsername())
				     .build());
	}

	@Override
	public Optional<UserProfile> approveUserAccount(String emailAddress, String byUserAccount) {

		Optional<User> user = this.userRepository.findOneByEmail(emailAddress);
		 if(! user.isPresent())
	     	return Optional.empty();
		 
		 user.get().setActivationKey(RandomUtil.generateAccountActivationKey());
		 user.get().setIsAccountEnabled(true);
		 user.get().setLastEnabledDateTime(Instant.now());
		 user.get().setLastEnabledBy(byUserAccount);
		 var usr = this.userRepository.save(user.get());
		 
		 
		return 	Optional.of(UserProfile.builder()
				                       .dateOfBirth(usr.getDateOfBirth()) 
				                       .email(usr.getEmail())
				                       .firstname(usr.getFirstName())
				                       .middlename(usr.getMiddleName())
				                       .phone(usr.getPhone())
				                       .surname(usr.getLastName())
				                       .username(usr.getLastName())
				                       .build()
				                       );
	}

	@Override
	public Optional<UserProfile> activateUserAccount(String activationKey) {
		
		Optional<User> user = this.userRepository.findOneByActivationKey(activationKey);
		 if(! user.isPresent())
	     	return Optional.empty();
		 
		 if( user.get().getIsAccountActivated())
			 return Optional.empty();
		 
		 user.get().setActivationKey(null);
		 user.get().setIsAccountActivated(true);
		 user.get().setLastActivatedDateTime(Instant.now());
		 user.get().setLastActivatedBy(user.get().getUsername());
		 
		 var usr = this.userRepository.save(user.get());
		 
		return Optional.of(UserProfile.builder()
				                       .dateOfBirth(usr.getDateOfBirth())
				                       .email(usr.getEmail())
				                       .firstname(usr.getFirstName())
				                       .middlename(usr.getMiddleName())
				                       .phone(usr.getPhone())
				                       .surname(usr.getLastName())
				                       .username(usr.getUsername())
				                    .build()
				                    ) ;
	}

	@Override
	public Optional<UserProfile> disableUserAccount(String emailAddress, String byUserAccount) {
		
		Optional<User> user = this.userRepository.findOneByEmail(emailAddress);
		 if(! user.isPresent())
	     	return Optional.empty();
		 
		 user.get().setActivationKey(null);
		 user.get().setIsAccountEnabled(false);
		 user.get().setIsAccountActivated(false);
		 user.get().setLastDiabledDateTime(Instant.now());
		 user.get().setLastDisabledBy(byUserAccount);
		 
		 var usr =  this.userRepository.save(user.get());
		 
		return Optional.ofNullable(UserProfile.builder()
				                               .activationKey(usr.getActivationKey())
				                               .accountIdentity(usr.getAccountIdentity())
				                               .dateOfBirth(usr.getDateOfBirth())
				                               .email(usr.getEmail())
				                               .firstname(usr.getFirstName())
				                               .middlename(usr.getMiddleName())
				                               .phone(usr.getPhone())
				                               .surname(usr.getLastName())
				                               .username(usr.getUsername())
				                              .build()) ;
	}

	@Override
	public Boolean enableUserAccount(Integer id, String byUserAccount) {
		
		Optional<User> user = this.userRepository.findById(null);
		 if(! user.isPresent())
	     	return false;
		 
		 user.get().setActivationKey(RandomUtil.generateAccountActivationKey());
		 user.get().setIsAccountEnabled(true);
		 user.get().setLastEnabledDateTime(Instant.now());
		 user.get().setLastDisabledBy(byUserAccount);
		 
		return this.userRepository.save(user.get()) == null ? false: true;
	}

	@Override
	public Optional<UserProfile> deactivateUserAccount(String emailAddress, String byUserAccount) {
		
		Optional<User> user = this.userRepository.findOneByEmail(emailAddress);
		 if(! user.isPresent())
	     	return Optional.empty();
		 
		 user.get().setActivationKey(null);
		 user.get().setIsAccountActivated(false);
		 user.get().setDeactivatedDate(Instant.now());
		 user.get().setLastDeactivatedBy(byUserAccount);
		 
		 var usr = this.userRepository.save(user.get());
		 
		 
		return Optional.ofNullable(UserProfile.builder()
				                               .activationKey(usr.getActivationKey())
				                               .accountIdentity(usr.getAccountIdentity())
				                               .dateOfBirth(usr.getDateOfBirth())
				                               .email(usr.getEmail())
				                               .firstname(usr.getFirstName())
				                               .middlename(usr.getMiddleName())
				                               .phone(usr.getPhone())
				                               .surname(usr.getLastName())
				                               .username(usr.getUsername())
				                               .companyId(usr.getCompany().getCompanyKey())
				                               .id(usr.getId())
				                               .password(usr.getPassword())
				                               
				                              .build()) ;
	}

	@Override
	public Boolean isUserNameTaken(String userName) {
		
		return this.userRepository.findOneByUsername(userName).isPresent() ? true: false;
	}

	@Override
	public Boolean isEmailAddressTaken(String emailAddress) {
		
		return this.userRepository.findOneByEmail(emailAddress).isPresent() ? true: false;
	}

	@Override
	public Optional<UserProfile> resetUserAccountPassword(String userNameOrEmail, String newPassword) {
		
		
		Optional<User> user = this.userRepository.findOneByUsernameOrEmail(userNameOrEmail, userNameOrEmail);
		if (! user.isPresent())
			return Optional.empty();	
		
		 if(user.get().getIsAccountEnabled() || ( !user.get().getIsAccountActivated()))
		 {
			 return Optional.empty();
		 }
		 
		 user.get().setPassword(passwordEncoder.encode(newPassword));
		 user.get().setLastPasswordResetDateTime(Instant.now());
	     var usr = this.userRepository.save(user.get());
		 
		return Optional.ofNullable( UserProfile.builder()
				                                .dateOfBirth(usr.getDateOfBirth())
				                                .email(usr.getEmail())
				                                .firstname(usr.getFirstName())
				                                .middlename(usr.getMiddleName())
				                                .phone(usr.getPhone())
				                                .surname(usr.getLastName())
				                                .username(usr.getUsername())
				                                .accountIdentity(usr.getAccountIdentity())
				                                .activationKey(usr.getActivationKey())
				                                .id(usr.getId())
				                                .companyId(usr.getCompany().getCompanyKey())
				                                .password(usr.getPassword())
				                               .build() );
	}

	@Override
	public Optional<UserProfile> getUserByAccountIdentity(String accountId) {
		
		Optional<User> result = this.userRepository.findOneByAccountIdentity(accountId);
		if (! result.isPresent())
			return Optional.empty();
		
		return Optional.of(UserProfile.builder()
                .accountIdentity(result.get().getAccountIdentity())
                .activationKey(result.get().getActivationKey())
                .dateOfBirth(result.get().getDateOfBirth())
                .email(result.get().getEmail())
                .firstname(result.get().getFirstName())
                .middlename(result.get().getMiddleName())
                .phone(result.get().getPhone())
                .surname(result.get().getLastName())
                .username(result.get().getUsername())
                .companyId(result.get().getCompany().getCompanyKey())
                .id(result.get().getId())
                .password(result.get().getPassword())
                .build());
	}

}