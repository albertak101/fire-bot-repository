package com.albert.finalYearProject.service.impl;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import com.albert.finalYearProject.entity.DataLogger;
import com.albert.finalYearProject.entity.Device;
import com.albert.finalYearProject.payload.DeviceApi;
import com.albert.finalYearProject.payload.DeviceDataLoggerRequest;
import com.albert.finalYearProject.payload.DeviceSensorDataApi;
import com.albert.finalYearProject.repository.DeviceSensorReadingsRepository;
import com.albert.finalYearProject.service.DeviceDataLoggerService;
import com.albert.finalYearProject.service.DeviceService;

@Service
public class DeviceDataLoggerServiceImpl implements DeviceDataLoggerService{

	
	@Autowired
	DeviceSensorReadingsRepository temperatureHumidityReadingRepository;
	
	@Autowired
	DeviceService deviceService;

	@Override
	public List<DeviceSensorDataApi> getAllReadings() {
		
		List<DataLogger> readings = Streamable.of(this.temperatureHumidityReadingRepository
                .findAll()).toList();
		
		List<DeviceSensorDataApi> readingResponses = readings.stream()
				                                 .map( reading -> { 
			                                	 DeviceSensorDataApi response = DeviceSensorDataApi.builder()
				                                	 						  .id(reading.getId())	
				                                			                  .readingTime(reading.getReadingTime())
				                                			                  .humidity(reading.getHumidity())
				                                			                  .temperature(reading.getTemperature())
				                                			                  .pressure(reading.getPressure())
				                                			                  .co2(reading.getCo2())
				                                			                  .altitude(reading.getAltitude())
				                                			                  .readingTime(reading.getDatabaseTime())
				                                			                  .build();
				                                	   return response;
				                                 }).collect(Collectors.toList());
				                                
		
		return readingResponses;
	}


	@Override
	public Optional<DeviceSensorDataApi> addDeviceReading(DeviceDataLoggerRequest reading) {
		
		Optional<DeviceApi> device = this.deviceService.getDeviceById(reading.getDeviceId());
		
		if( ! device.isPresent())
		    return Optional.empty();
		
		DataLogger remoteReading =  DataLogger.builder()
				                              .readingTime(reading.getReadingTime())
				                              .humidity(reading.getHumidity())				                                     
				                              .temperature(reading.getTemperature())
				                              .pressure(reading.getPressure())
				                              .co2(reading.getCo2())
				                              .altitude(reading.getAltitude())
				                              .databaseTime(Instant.now())	
				                              .device(Device.builder()
				                            		         .deviceId(device.get().getDeviceId())
				                            		         .deviceKey(device.get().getDeviceKey())
				                            		         .deviceName(device.get().getDeviceName())
				                            		         .id(device.get().getId())
				                            		         .description(device.get().getDescription())
				                            		         .build())
				                              .build();
		
		DataLogger result =	this.temperatureHumidityReadingRepository.save(remoteReading);
	
	 return	Optional.of(DeviceSensorDataApi.builder()
				                .readingTime(result.getReadingTime())
				                .humidity(result.getHumidity())
				                .temperature(result.getTemperature())
				                .pressure(result.getPressure())
				                .co2(result.getCo2())
				                .altitude(reading.getAltitude())
				                .deviceId(result.getDevice().getDeviceId())
				                
				                .build());
	
	}

//
//	@Override
//	public Optional<DataLoggerResponse> getReadingById(Integer id) {
//	
//	 Optional<DataLogger> result = this.temperatureHumidityReadingRepository.getAllById(id);
//		if( result.isEmpty())
//		   return Optional.empty();
//		
//		DataLoggerResponse reading = DataLoggerResponse .builder()
//				.id(result.get().getId())
//                .readingTime(result.get().getReadingTime())
//                .humidity(result.get().getHumidity())
//               // .mcuId(result.get().getMcuId())
//                .temperature(result.get().getTemperature())
//                .pressure(result.get().getPressure())
//                //.co2(result.get().getCo2())
//                .altitude(result.get().getAltitude())
//                //.sensorId(result.get().getSensorId())
//                .build();
//		
//		return Optional.ofNullable(reading);
//	}

//
//	@Override
//	public List<DataLoggerResponse> getReadingsByMcuId(String id) {
//		List<DataLogger> readings = Streamable.of(this.temperatureHumidityReadingRepository
//                .getAllByMcuId(id)).toList();
//				
//		
//		List<DataLoggerResponse> readingResponses = readings.stream()
//				                                 .map( reading -> { 
//				                                	 					DataLoggerResponse response = DataLoggerResponse.builder()
//				                                	 						  .id(reading.getId())
//				                                			                  .readingTime(reading.getReadingTime())
//				                                			                  .humidity(reading.getHumidity())
//				                                			                  //.mcuId(reading.getMcuId())
//				                                			                 // .sensorId(reading.getSensorId())
//				                                			                  .temperature(reading.getTemperature())
//				                                			                  .pressure(reading.getPressure())
//				                                			                  .altitude(reading.getAltitude())
//				                                			                
//				                                			                  .build();
//				                                	   return response;
//				                                 }).collect(Collectors.toList());
//				                                
//		
//		return readingResponses;
//
//}
//	
//	
	
}
