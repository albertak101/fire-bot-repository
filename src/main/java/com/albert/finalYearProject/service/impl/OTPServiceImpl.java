package com.albert.finalYearProject.service.impl;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.albert.finalYearProject.service.OTPService;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
@Primary
public class OTPServiceImpl implements OTPService {


	private static final Integer EXPIRE_MINS = 15;

	private LoadingCache<String, Integer> otpCache;
	
	

	public OTPServiceImpl() {
		super();
		
		this.otpCache = CacheBuilder.newBuilder().expireAfterWrite(EXPIRE_MINS, TimeUnit.MINUTES)
				.build(new CacheLoader<String, Integer>() {
					public Integer load(String key) {
						return 0;
					}
				});
	}

	@Override
	public Integer generateOTPKey(String otpKey) {
	
		Random random = new Random();
		int otp = 100000 + random.nextInt(900000);
		otpCache.put(otpKey, otp);
		log.info(otpCache.toString());
		
		return otp;
	}

	@Override
	public Boolean validateOTPkey(String optKey) {
		try {
			return otpCache.get(optKey) > 0 ? true: false;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public Integer getOTPFromKey(String otpKey) {
		try {
			return otpCache.get(otpKey);
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public void clearOTP(String otpKey) {
		
		otpCache.invalidate(otpKey);
		
	}

	@Override
	public String getKeyFromOtp(Integer otp) {
		
		ConcurrentHashMap<String, Integer>  map =  (ConcurrentHashMap<String, Integer>) this.otpCache.asMap();
		
		map.forEachKey(0, null, null);
		//this.otpCache.
		return null;
	}


}
