package com.albert.finalYearProject.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import com.albert.finalYearProject.entity.Company;
import com.albert.finalYearProject.payload.CompanyApi;
import com.albert.finalYearProject.payload.CompanyLookUp;
import com.albert.finalYearProject.payload.CreateCompanyRequest;
import com.albert.finalYearProject.repository.CompanyRepository;
import com.albert.finalYearProject.service.CompanyService;
import com.albert.finalYearProject.utils.RandomUtil;

@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private  CompanyRepository companyRepository;
	 
	@Override
	public List<CompanyApi> getAllCompanies() {
		
	  List<Company> resultset = Streamable.of(this.companyRepository.findAll()).toList()	;
	    
			return  resultset.stream()
				.map(r -> {
					  return CompanyApi.builder()
							  .id(r.getId()) 
							  .companyName(r.getCompanyName())
							  .comment(r.getComment())
							  .companyKey(r.getCompanyKey())
							  .companyAccountNumber(r.getCompanyAccountNumber())
							  .contactEmailAddress(r.getContactEmailAddress())
							  .contactPersonName(r.getContactPersonName())
							  .contactTelephone(r.getContactTelephone())
							  .build();
				}).collect(Collectors.toList());
	}

	@Override
	public Optional<CompanyApi> addNewCompany(CreateCompanyRequest request) {
		 
		Company company = Company.builder()
				                 .companyName(request.getCompanyName())
				                 .comment(request.getComment())
				                 .companyAccountNumber(RandomUtil.generateCompanyAccountNumber())
				                 .companyKey(RandomUtil.generateCompanyRegistrationKey())
				                 .contactEmailAddress(request.getContactEmailAddress())
				                 .contactTelephone(request.getContactTelephone())
				                 .contactPersonName(request.getContactPersonName())
				                 .build();
		
	
		Company createdCompany = this.companyRepository.save(company);
		
		return Optional.ofNullable(CompanyApi.builder()
				                              .id(createdCompany.getId())
				                              .comment(createdCompany.getComment())
				                              .companyName(createdCompany.getCompanyName())
				                              .companyKey(createdCompany.getCompanyKey())
				                              .companyAccountNumber(createdCompany.getCompanyAccountNumber())
				                              .contactEmailAddress(createdCompany.getContactEmailAddress())
				                              .contactTelephone(createdCompany.getContactTelephone())
				                              .contactPersonName(createdCompany.getContactPersonName())
				                              .build());
	}

	@Override
	public Optional<CompanyApi> updateCompanyDetails(CompanyApi companyApi) {
		
		Optional<Company> company = this.companyRepository.findOneByCompanyKey(companyApi.getCompanyKey());
		if (! company.isPresent())
			return Optional.empty();
		
		company.get().setComment(companyApi.getComment());
		company.get().setCompanyName(companyApi.getCompanyName());
		company.get().setContactEmailAddress(companyApi.getContactEmailAddress());
		company.get().setContactTelephone(companyApi.getContactTelephone());
		
	    Company savedCompany = this.companyRepository.save(company.get());	    
	    
		return Optional.ofNullable( CompanyApi.builder()
				                               .id(savedCompany.getId())
				                               .comment(savedCompany.getComment())
				                               .companyName(savedCompany.getCompanyName())
					                           .companyKey(savedCompany.getCompanyKey())
					                           .companyAccountNumber(savedCompany.getCompanyAccountNumber())
					                           .contactEmailAddress(savedCompany.getContactEmailAddress())
					                           .contactTelephone(savedCompany.getContactTelephone())
					                           .contactPersonName(savedCompany.getContactPersonName())
				                               .build());
	}

	@Override
	public Boolean delistCompany(String companyId) {

        Optional<Company> company = this.companyRepository.findOneByCompanyKey(companyId);
         
        if( ! company.isPresent())
        	return false;
        this.companyRepository.delete(company.get());
        
		return true;
	}

	@Override
	public Optional<CompanyApi> getCompanyDetails(String companyKey) {
	
		Optional<Company> company = this.companyRepository.findOneByCompanyKey(companyKey);
		if(! company.isPresent())
			  return Optional.empty();
		
		return Optional
				    .of(CompanyApi.builder()
				    		      .comment(company.get().getComment())
				    		      .companyAccountNumber(company.get().getCompanyAccountNumber())
				    		      .companyKey(company.get().getCompanyKey())
				    		      .companyName(company.get().getCompanyName())
				    		      .contactEmailAddress(company.get().getContactEmailAddress())
				    		      .contactPersonName(company.get().getContactPersonName())
				    		      .contactTelephone(company.get().getContactTelephone())
				    		      .build());
	}

	@Override
	public List<CompanyLookUp> getAllCompanyList() {
		List<Company> resultset = Streamable.of(this.companyRepository.findAll()).toList()	;
		

		return  resultset.stream()
			.map(r -> {
				  return CompanyLookUp.builder()
						
						  .companyName(r.getCompanyName())
						  .companyKey(r.getCompanyKey())
						  .build();
			}).collect(Collectors.toList());
	}

	

}
