package com.albert.finalYearProject.service;

import java.util.List;
import java.util.Optional;

import com.albert.finalYearProject.entity.Device;
import com.albert.finalYearProject.payload.AddDeviceRequest;
import com.albert.finalYearProject.payload.AddDeviceResponse;
import com.albert.finalYearProject.payload.DeviceApi;
import com.albert.finalYearProject.payload.DeviceUpdateRequest;


public interface DeviceService {
	
	Optional<AddDeviceResponse> addNewDevice(AddDeviceRequest deviceRequest);
	
	List<DeviceApi> getAllDevices();
	Optional<DeviceApi> getDeviceById(String deviceId);
	//Boolean deleteDeviceById(Long id);
    //Boolean deleteDeviceByDeviceId(String deviceId);
	List <Device>getDeviceByOwner(Long companyId);
	//List<Device> getDevicesByDeviceId(String deviceId);
	
	
	List<DeviceApi> getAllCompanyDevices(String companyKey);
    Optional<DeviceApi> updateCompanyDevice(String companyId, String deviceId, DeviceUpdateRequest deviceUpdateRequest);
	Optional<DeviceApi> getCompanyDeviceByDeviceId(String companyKey, String deviceId);
	
	Boolean deleteCompanyDeviceById(String companyId, String deviceId);
	//Boolean deleteCompanyDeviceByDeviceKey(Long companyId, String deviceId);
	
    //Boolean deleteCompanyDeviceById(Long companyId, Long deviceId);	
	Boolean deregisterDevice( String companyId, String deviceId);
	Boolean deactivateDevice( String companyId, String deviceId);
	Optional<DeviceApi> activateDevice(String activationKey);
	
	Optional<DeviceApi> approveDeviceRegistration(String deviceId);
	Optional<DeviceApi> disableDevice(String deviceId);
	
	
	
}
