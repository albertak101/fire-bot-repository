package com.albert.finalYearProject.service;

import java.util.List;
import java.util.Optional;

import com.albert.finalYearProject.payload.DeviceDataLoggerRequest;
import com.albert.finalYearProject.payload.DeviceSensorDataApi;

public interface DeviceDataLoggerService {
	
	Optional<DeviceSensorDataApi> addDeviceReading(DeviceDataLoggerRequest reading);	
	List<DeviceSensorDataApi> getAllReadings();

}
