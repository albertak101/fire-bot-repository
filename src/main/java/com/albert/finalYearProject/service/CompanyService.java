package com.albert.finalYearProject.service;

import java.util.List;
import java.util.Optional;

import com.albert.finalYearProject.payload.CompanyApi;
import com.albert.finalYearProject.payload.CompanyLookUp;
import com.albert.finalYearProject.payload.CreateCompanyRequest;

public interface CompanyService {
	
	List<CompanyApi> getAllCompanies ();
	Optional<CompanyApi> addNewCompany(CreateCompanyRequest request);
	Optional<CompanyApi> updateCompanyDetails(CompanyApi company);
	Optional<CompanyApi> getCompanyDetails(String companyKey);
	Boolean delistCompany(String companyId);
	List<CompanyLookUp> getAllCompanyList();

}
