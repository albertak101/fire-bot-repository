package com.albert.finalYearProject.api;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.albert.finalYearProject.core.EmailMessageDetail;
import com.albert.finalYearProject.integration.email.services.EmailIntegrationServices;
import com.albert.finalYearProject.payload.EmptyJsonObjectPayload;
import com.albert.finalYearProject.payload.UserProfile;
import com.albert.finalYearProject.service.UserService;


@RestController
@RequestMapping("api/v1/user-management/users")
@CrossOrigin
public class UserManagementApiEndPoint {
	
	@Autowired
	private UserService  userService ;
	
	@Autowired
	private  EmailIntegrationServices  emailService ;
	
	@GetMapping("")
	  public ResponseEntity<List<UserProfile>> getAllUsers() {
	  List<UserProfile> response = userService.getAllUsers();
	  return ResponseEntity.ok(response);
	}
	
	  @DeleteMapping("user/{userid}")
	  public ResponseEntity<?> deleteUser(@PathVariable String userid ) {
	
		  Boolean response = userService.deleteUser(userid);
		  
		  if(response)
		     return ResponseEntity.notFound().build();
		    		 
	      return ResponseEntity.noContent().build();
	}
	  

	  @GetMapping("user/{id}")
	  public ResponseEntity<Optional<UserProfile>> getUserById(@PathVariable Long id) {
	  Optional<UserProfile> response = userService.getUserById(id);
	  return ResponseEntity.ok(response);
	
	}


	  @GetMapping("user/profile/{username}")
	  public ResponseEntity<?> getUserByUsername(@PathVariable String username) {
	 
		  Optional<UserProfile> response = userService.getUserProfile(username);
		  
		  if(! response.isPresent()) {
			  
			  return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new EmptyJsonObjectPayload());
		  }
	 
	     return ResponseEntity.ok(response.get());
	}
	  
	
	    @PutMapping("user/account-approval/{email}")
	    public ResponseEntity<?> approveUserAccount(@PathVariable("email") String emailAddress)
	    {
	    		    	
	    	Optional<UserProfile> user = this.userService.approveUserAccount(emailAddress, "");
	    	
	    	if(! user.isPresent())
	    		return ResponseEntity
	    				      .status(HttpStatus.BAD_REQUEST)
	    				      .contentType(MediaType.TEXT_HTML)
	    				      .body("<b> This Account Does Not Exit or Has Already Been Activated");
	    	
	        // send Account Activation mail
	        
	        EmailMessageDetail email = EmailMessageDetail.builder()
	        		                                      .firstName(user.get().getFirstname())
	        		                                      .fromAddress("iot@iotteam.com.gh")
	        		                                      .lastName(user.get().getSurname())
	        		                                      .subject("Account Activation")
	        		                                      .toAddress(user.get().getEmail())
	        		                                      .build();
	        
			URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
					.path("/public/account-activation/activation-key/{activationkey}")
					.buildAndExpand(user.get().getActivationKey())
					.toUri();
	        
	 
	    	this.emailService.dispatchAccountActivationEmail(email, location.toString());
	    	return  ResponseEntity.ok().build();
		        
	    }  
	    
	    @DeleteMapping("user/account-disable/{email}")
	    public ResponseEntity<?> disableUserAccount(@PathVariable("email") String emailAddress) {
	    	
	    	Optional<UserProfile> user = this.userService.disableUserAccount(emailAddress, "Admin");
	    	
	    	if( ! user.isPresent())
	    	   return ResponseEntity.notFound().build();
	    	
	        // send Account Activation mail
	        
	        EmailMessageDetail email = EmailMessageDetail.builder()
	        		                                      .firstName(user.get().getFirstname())
	        		                                      .fromAddress("iot@iotteam.com.gh")
	        		                                      .lastName(user.get().getSurname())
	        		                                      .subject("Account Disabled")
	        		                                      .toAddress(user.get().getEmail())
	        		                                      .build();
	        
	        //Send Account Disable Email
	        
	        this.emailService.dispatchAccountDisabledEmailMessage(email);
	        
	      return   ResponseEntity.ok(null);
	    	
	    	
	    }
	    
	    
	    @PostMapping("user/account-deactivate/{email}")
	    public ResponseEntity<?> deactivateUserAccount(@PathVariable("email") String emailAddress) {
	    
	    	Optional<UserProfile> user = this.userService.deactivateUserAccount(emailAddress, "Admin");
	    	
	    	if( ! user.isPresent())
	    	   return ResponseEntity.notFound().build();
	    	
	        // send Account Activation mail
	        
	        EmailMessageDetail email = EmailMessageDetail.builder()
	        		                                      .firstName(user.get().getFirstname())
	        		                                      .fromAddress("iot@iotteam.com.gh")
	        		                                      .lastName(user.get().getSurname())
	        		                                      .subject("Account Deactivation")
	        		                                      .toAddress(user.get().getEmail())
	        		                                      .build();
	        
	        //Send Account Disable Email
	        
	        this.emailService.dispatchAccountDeactivatedEmailMessage(email);
	        
	      return   ResponseEntity.ok().build();
	    }
	
	

}
