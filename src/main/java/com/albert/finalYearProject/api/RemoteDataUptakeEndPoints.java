package com.albert.finalYearProject.api;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.albert.finalYearProject.payload.DeviceDataLoggerRequest;
import com.albert.finalYearProject.payload.DeviceDataLoggerResponse;
import com.albert.finalYearProject.payload.DeviceSensorDataApi;
import com.albert.finalYearProject.service.DeviceDataLoggerService;


@RestController
@RequestMapping("api/v1/devices")
@CrossOrigin
public class RemoteDataUptakeEndPoints {
	
	@Autowired
	DeviceDataLoggerService  temperatureHumidityReadingService ;
	
	
	@PostMapping("device/sensor-data")  
	public ResponseEntity <DeviceDataLoggerResponse> uploadDeviceSensorData(@Validated @RequestBody DeviceDataLoggerRequest deviceSensorData) {	
		try {
			  
			    Optional<DeviceSensorDataApi> data = temperatureHumidityReadingService.addDeviceReading(deviceSensorData);
			   if(! data.isPresent())
			          return ResponseEntity.badRequest().body(new DeviceDataLoggerResponse("400","Could Not Upload Device Sensor Data"));
			
			 	return new ResponseEntity<>(new DeviceDataLoggerResponse("201","Device Sensor Data Uploaded Successfully"), HttpStatus.CREATED);
	    	 } catch (Exception e) {
		      return new ResponseEntity<>(new DeviceDataLoggerResponse("500","Error Uploading Device Sensor Data. Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		    }
		}
	

	@GetMapping("/sensor-data")
	  public ResponseEntity<List<DeviceSensorDataApi>> getAllDevicesSensorData() {
		
	  List<DeviceSensorDataApi> reading = temperatureHumidityReadingService.getAllReadings();
	  return ResponseEntity.ok(reading);
	}
	
	
	
	
	
	
	
//	
//	@CrossOrigin
//	  @GetMapping("/sensor/temperature/{id}")
//	  public ResponseEntity<Optional<DataLoggerResponse>> getReadingsById(@PathVariable Integer id) {
//	  Optional<DataLoggerResponse> reading = temperatureHumidityReadingService.getReadingById(id);
//	  return ResponseEntity.ok(reading);
//	}
//	
//	
//	  
//	
//	  
//	@CrossOrigin
//	  @GetMapping("/sensor/{deviceId}")
//	  public ResponseEntity<List<DataLoggerResponse>> getReadingsByMcuId(@PathVariable String deviceId) {
//	  List<DataLoggerResponse> reading = temperatureHumidityReadingService.getReadingsByMcuId(deviceId);
//	  return ResponseEntity.ok(reading);
//	
//	}
	
	
}
	

	

  