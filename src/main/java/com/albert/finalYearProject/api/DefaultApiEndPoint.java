package com.albert.finalYearProject.api;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.albert.finalYearProject.core.EmailMessageDetail;
import com.albert.finalYearProject.integration.email.services.EmailIntegrationServices;
import com.albert.finalYearProject.payload.CompanyLookUp;
import com.albert.finalYearProject.payload.CreateUserRequest;
import com.albert.finalYearProject.payload.CreateUserResponse;
import com.albert.finalYearProject.payload.DeviceApi;
import com.albert.finalYearProject.payload.EmailAvailabilityResponse;
import com.albert.finalYearProject.payload.EmptyJsonObjectPayload;
import com.albert.finalYearProject.payload.LoginResponse;
import com.albert.finalYearProject.payload.OtpValidationRequest;
import com.albert.finalYearProject.payload.UserProfile;
import com.albert.finalYearProject.payload.UsernameAvailabilityResponse;
import com.albert.finalYearProject.security.jwt.JwtTokenProvider;
import com.albert.finalYearProject.service.CompanyService;
import com.albert.finalYearProject.service.DeviceService;
import com.albert.finalYearProject.service.OTPService;
import com.albert.finalYearProject.service.UserService;
import com.albert.finalYearProject.utils.RandomUtil;
import com.fasterxml.jackson.core.JsonProcessingException;




@CrossOrigin
@RequestMapping("public")
@RestController
public class DefaultApiEndPoint {
	
	
	@Autowired
	UserService  userService ;
	
	@Autowired
	EmailIntegrationServices  emailService ;
	
	@Autowired
	private OTPService otpService; 
	
	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	
	@Autowired
	DeviceService deviceService;
	
	@Autowired
	CompanyService companyService;

	
	@GetMapping(path = "ping", produces= MediaType.TEXT_PLAIN_VALUE)	
	public ResponseEntity <String> doPingPong() {
    	return  ResponseEntity
    			        .ok()
    			        .body("Pong")
    			        ;
    			       
    }
	
	@GetMapping(path="server-date-time",  produces= MediaType.TEXT_PLAIN_VALUE)	
	public ResponseEntity <String> getServerDateTime() {
    	return  ResponseEntity
    			        .ok()
    			        .body(Instant.now().toString())
    			        ;
    			       
    }
	
	
	@GetMapping("/")
	public ResponseEntity<String> doDisplayIndexPage() {
		
		return  ResponseEntity.ok("IOT Application Server. Powered By Meta Web Technologies Ghana Limited. (c) 2021-2028");
	}
	
	
	@PostMapping("sign-up")  
	public ResponseEntity <CreateUserResponse> userRegistration(@Validated @RequestBody CreateUserRequest userRequest) {	
		try {
			
			if( userService.isEmailAddressTaken(userRequest.getEmail())) {
				
				  return ResponseEntity.badRequest().build();
			}
			
			if( userService.isUserNameTaken(userRequest.getUsername())) {
				
				  return ResponseEntity.badRequest().build();
			}
			
			CreateUserResponse _createUserResponse = 
			userService.addNewUser(userRequest);
			
			EmailMessageDetail email = EmailMessageDetail.builder()
					.firstName(userRequest.getFirstname())
					.fromAddress("iot@iotteam.com.gh")
					.lastName(userRequest.getSurname())
					.subject("User Registration Confirmation")
					.toAddress(userRequest.getEmail())
					.userName(userRequest.getUsername())
					.build();
			
			emailService.dispatchUserAccessAccountRegistrationEmail(email, _createUserResponse);
			 
			return new ResponseEntity<>(_createUserResponse, HttpStatus.CREATED);
		 } catch (Exception e) {
		      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		    }
    	}
	
	
	  @GetMapping("forgot-password/{email}")
	  public ResponseEntity<?> recoverUserPassword(@PathVariable String email) {
	
		  String response = userService.rememberUserPassword(email);
		  if (response.length() == 0)
 	          return 
 	        		ResponseEntity
 	        		    .badRequest()
 	        		    .build();
		  
		  EmailMessageDetail msg = EmailMessageDetail.builder()
					.firstName("")
					.fromAddress("iot@iotteam.com.gh")
					.lastName("")
					.subject("User onfirmation- OTP Code")
					.toAddress(email)
					.userName("")
					.build();
		  
		     emailService.dispatchOtpEmail(msg, email, response);
		     return 
	 	        		ResponseEntity
	 	        		    .ok(response);
	
	}
	  

	    @GetMapping("account-activation/activation-key/{activationkey}")
	    public ResponseEntity<?> activateUserAccount(@PathVariable("activationkey") String activationKey)
	    {
	    		    	
	    	Optional<UserProfile> user = this.userService.activateUserAccount(activationKey);
	    	
	    	if(! user.isPresent())
	    		return ResponseEntity
	    				      .status(HttpStatus.BAD_REQUEST)
	    				      .contentType(MediaType.TEXT_HTML)
	    				      .body("<b> This Account Does Not Exit or Has Already Been Activated");
	    	
	        // send Account Activation mail
	        
	        EmailMessageDetail email = EmailMessageDetail.builder()
	        		                                      .firstName(user.get().getFirstname())
	        		                                      .fromAddress("iot@iotteam.com.gh")
	        		                                      .lastName(user.get().getSurname())
	        		                                      .subject("User Login Details")
	        		                                      .toAddress(user.get().getEmail())
	        		                                      .build();
	        
	        var loginUrl = "";
	    	this.emailService.dispatchAccountWelcomeEmail(email, loginUrl);
	    	return  ResponseEntity.status(HttpStatus.OK)
		               
		               .body("<b> User Account Successfully Activated. Please Check Your Email For Account Details </b>");
	    }
	    
	    
	    
	    @GetMapping("device-activation/activation-key/{activationkey}")
	    public ResponseEntity<?> activateDevice(@PathVariable("activationkey") String activationKey)
	    {
	    		    	
	    	Optional<DeviceApi> device = this.deviceService.activateDevice(activationKey);
	    	
	    	if(! device.isPresent())
	    		return ResponseEntity
	    				      .status(HttpStatus.BAD_REQUEST)
	    				      .contentType(MediaType.TEXT_HTML)
	    				      .body("<b> This Device Does Not Exit or Has Already Been Activated");
	    	
	       

	    	return  ResponseEntity.status(HttpStatus.OK)
		               
		               .body("<b> Device Successfully Activated. Welcome to FireBot IOT </b>");
	    }
	    
	    @PostMapping("sign-in")
	    public ResponseEntity<?> authenticateUser(@Valid @RequestBody com.albert.finalYearProject.payload.LoginRequest loginRequest, BindingResult validationErrors) throws JsonProcessingException {
	    	
	        Authentication authentication = authenticationManager.authenticate(
	                new UsernamePasswordAuthenticationToken(
	                        loginRequest.getUserNameOrEmail(),
	                        loginRequest.getPassword()
	                )
	        );

	        SecurityContextHolder.getContext().setAuthentication(authentication);

	        String accessToken = jwtTokenProvider.generateJWTAccessToken(authentication);
	        String refreshToken = jwtTokenProvider.generateRefreshToken();
	    	
	    	return ResponseEntity.ok( LoginResponse.builder()
	    			                               .accessToken(accessToken) 
	    			                               .refreshToken(refreshToken)
	    			                               .email( loginRequest.getUserNameOrEmail())
	    			                               .build());
	    	
	    
	    }
	    
	    
	    @GetMapping("check-username-availability/username/{username}")
	    public ResponseEntity<UsernameAvailabilityResponse> checkUsernameAvailability(@PathVariable("username") String username) {
	    	
	    
	        return  ResponseEntity.ok(new UsernameAvailabilityResponse(this.userService.isUserNameTaken(username)));
	       
	    }

	    @GetMapping("check-email-availability/email/{email}")
	    public ResponseEntity<EmailAvailabilityResponse> checkEmailAvailability(@PathVariable("email") String emailAddress) {
	    	
	        return ResponseEntity.ok( new EmailAvailabilityResponse(this.userService.isEmailAddressTaken(emailAddress)));
	    }
	    
	    
		
		  @PostMapping("otp-validate/{otp}")  
		  public ResponseEntity <?> validateOtp(@Validated @RequestBody OtpValidationRequest otpRequest) {	
				try {
					
			           if (!this.otpService.validateOTPkey(otpRequest.otpCode.toString())) {
			        	   	   return 
			    	        		ResponseEntity
			    	        		    .status(HttpStatus.NOT_FOUND)
			    	        		    .body(new EmptyJsonObjectPayload());
			        	   
			           }
					Optional<UserProfile> userProfile = this.userService.getUserProfile(otpRequest.getEmailAddress());
					
					if( !userProfile.isPresent()) 
					{
						 return 
			    	           ResponseEntity
			    	        		.status(HttpStatus.NOT_FOUND)
			    	        		 .body(new EmptyJsonObjectPayload());
					}
					
					String newPassword = RandomUtil.generatePassword();
					var usr = this.userService.resetUserAccountPassword(newPassword, newPassword);
					if ( ! usr.isPresent())
					{
						 return 
				    	           ResponseEntity
				    	        		.status(HttpStatus.NOT_FOUND)
				    	        		 .body(new EmptyJsonObjectPayload());		 
		
					}
					
					EmailMessageDetail email = EmailMessageDetail.builder()
							.firstName(usr.get().getFirstname())
							.fromAddress("iot@iotteam.com.gh")
							.lastName(usr.get().getSurname())
							.subject("User Registration Confirmation")
							.toAddress(usr.get().getEmail())
							.userName(usr.get().getUsername())
							.build();
					
					emailService.dispatchUserAccountPasswordResetEmail(email, newPassword);
					 
					return  ResponseEntity.ok(usr.get());
				 } catch (Exception e) {
				      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
				    }
		    	}	
		  
		  @GetMapping("company-lookup")
		  public ResponseEntity<List<CompanyLookUp>> getCompanyLookUp() {
		  List<CompanyLookUp> response = companyService.getAllCompanyList();
		  return ResponseEntity.ok(response);
		}
	  
}
