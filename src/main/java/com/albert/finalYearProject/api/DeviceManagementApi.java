package com.albert.finalYearProject.api;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.albert.finalYearProject.core.EmailMessageDetail;
import com.albert.finalYearProject.integration.email.services.EmailIntegrationServices;
import com.albert.finalYearProject.payload.AddDeviceRequest;
import com.albert.finalYearProject.payload.AddDeviceResponse;
import com.albert.finalYearProject.payload.CompanyApi;
import com.albert.finalYearProject.payload.DeviceApi;
import com.albert.finalYearProject.service.CompanyService;
import com.albert.finalYearProject.service.DeviceService;

@RestController
@RequestMapping("api/v1/device-management")
@CrossOrigin
public class DeviceManagementApi {
	
	@Autowired
	DeviceService  deviceService ;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	EmailIntegrationServices emailIntegrationServices;
	
	@GetMapping("devices")
	  public ResponseEntity<List<DeviceApi>> getAllDevices() {
	  List<DeviceApi> response = deviceService.getAllDevices();
	  return ResponseEntity.ok(response);
	}
	
	
	  @PutMapping("devices/approve-device/device/{deviceid}")
	  public ResponseEntity<?> approveDivice(@PathVariable("deviceid") String deviceId) {
	 
		  Optional<DeviceApi> response = deviceService.approveDeviceRegistration(deviceId);
		  
		  if(! response.isPresent())
		       return ResponseEntity.badRequest().build();
		  
		  //Get Company Details
		  
		  Optional<CompanyApi> company = this.companyService.getCompanyDetails(response.get().getCompanyId());
		  
		  if(! company.isPresent())
		       return ResponseEntity.badRequest().build();
		  
		  // send Account Activation mail
	        
	        EmailMessageDetail email = EmailMessageDetail.builder()
	        		                                      .firstName(company.get().getContactPersonName())
	        		                                      .fromAddress("iot@iotteam.com.gh")
	        		                                      .lastName("")
	        		                                      .subject("Device Activation")
	        		                                      .toAddress(company.get().getContactEmailAddress())
	        		                                      .build();
	        
			URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
					.path("/public/device-activation/activation-key/{activationkey}")
					.buildAndExpand(response.get().getActivationKey())
					.toUri();
			
			this.emailIntegrationServices.dispatchDeviceActivationEmail(email, response.get().getDeviceId(), location.toString());
		  
	     return ResponseEntity
	    		       .ok().build();
	
	}
	  
	@PostMapping("/devices/device")  
	public ResponseEntity <AddDeviceResponse> registerCompanyDevice(@Validated @RequestBody AddDeviceRequest deviceRequest, @PathVariable("companyname") String companyName ) {	
			try {
				
				System.out.println(" Got Data " + deviceRequest.toString());
				Optional<AddDeviceResponse> _deviceResponse = 
				deviceService.addNewDevice(deviceRequest);
				 
				return new ResponseEntity<>(_deviceResponse.get(), HttpStatus.CREATED);
			 } catch (Exception e) {
			      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			    }
		}  

}
