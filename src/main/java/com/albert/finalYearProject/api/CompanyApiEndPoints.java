package com.albert.finalYearProject.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.albert.finalYearProject.core.EmailMessageDetail;
import com.albert.finalYearProject.integration.email.services.EmailIntegrationServices;
import com.albert.finalYearProject.payload.CompanyApi;
import com.albert.finalYearProject.payload.CreateCompanyRequest;
import com.albert.finalYearProject.payload.EmptyJsonObjectPayload;
import com.albert.finalYearProject.service.CompanyService;

@RestController
@CrossOrigin
@RequestMapping("api/v1/companies")
public class CompanyApiEndPoints {
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	EmailIntegrationServices  emailService ;
	
	@PostMapping("company")  
	public ResponseEntity<?> createNewCompany( @Validated @RequestBody CreateCompanyRequest request) {
		
		Optional<CompanyApi> newCompany = this.companyService.addNewCompany(request);
		
		  if ( !newCompany.isPresent())
		      return  ResponseEntity
		    		       .status(HttpStatus.NOT_FOUND)
		    		       .body(new EmptyJsonObjectPayload());
		  
		  EmailMessageDetail email = EmailMessageDetail.builder()
					.firstName(request.getContactPersonName())
					.fromAddress("iot@iotteam.com.gh")
					.lastName("")
					.subject("Company Registration Confirmation")
					.toAddress(request.getContactEmailAddress())
					.userName(request.getCompanyName())
					.build();
		  
		this.emailService.dispatchCompanyRegistrationEmail(email, newCompany.get());  
		
		return  ResponseEntity	
				    .status(HttpStatus.CREATED)
				    .body(newCompany.get());
	}
	
	@PutMapping("company")  
	public ResponseEntity<?> updateCompany( @Validated @RequestBody CompanyApi company) {
		
		Optional<CompanyApi> newCompany = this.companyService.updateCompanyDetails(company);
		
		  if ( !newCompany.isPresent())
		      return  ResponseEntity
		    		       .status(HttpStatus.NOT_FOUND)
		    		       .body(new EmptyJsonObjectPayload());
		
		return  ResponseEntity				   
				    .ok(newCompany.get());
				
	}
	
	@GetMapping("")  
	public ResponseEntity<List<CompanyApi>> getAllCompanies() {
		
		return  ResponseEntity
				     .ok(companyService.getAllCompanies());
	}
	

}
