package com.albert.finalYearProject.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.albert.finalYearProject.core.EmailMessageDetail;
import com.albert.finalYearProject.integration.email.services.EmailIntegrationServices;
import com.albert.finalYearProject.payload.AddDeviceRequest;
import com.albert.finalYearProject.payload.AddDeviceResponse;
import com.albert.finalYearProject.payload.CompanyApi;
import com.albert.finalYearProject.payload.DeviceApi;
import com.albert.finalYearProject.payload.DeviceUpdateRequest;
import com.albert.finalYearProject.payload.EmptyJsonObjectPayload;
import com.albert.finalYearProject.service.CompanyService;
import com.albert.finalYearProject.service.DeviceService;

@RestController
@CrossOrigin
@RequestMapping("api/v1/company/{companyid}")
public class DeviceApiEndPoints {
	
	@Autowired
	DeviceService  deviceService ;

	@Autowired
	EmailIntegrationServices emailService;
	
	@Autowired
	CompanyService companyService;
	
	
	@PostMapping("/devices/device")  
	public ResponseEntity <AddDeviceResponse> registerCompanyDevice(@Validated @RequestBody AddDeviceRequest deviceRequest, @PathVariable("companyid") String companyKey ) {	
		try {
			
			System.out.println(" Got Data " + deviceRequest.toString());
			
			Optional<CompanyApi> company = this.companyService.getCompanyDetails(companyKey);
			
		   if(! company.isPresent()) {
				
				return ResponseEntity.badRequest().build();
			}
            
		   Optional<AddDeviceResponse> deviceResponse = 
					deviceService.addNewDevice(deviceRequest);
					
					if(! deviceResponse.isPresent()) {
						
						return ResponseEntity.internalServerError().build();
					}
					
			EmailMessageDetail msg = EmailMessageDetail.builder()
						.firstName(company.get().getContactPersonName())
						.fromAddress("iot@iotteam.com.gh")
						.lastName("")
						.subject("Device Registration Confirmation")
						.toAddress(company.get().getContactEmailAddress())
						.userName(company.get().getCompanyName())
						.build();
			
			 this.emailService.dispatchDeviceRegistrationEmail(msg, deviceResponse.get());
			 
			return new ResponseEntity<>(deviceResponse.get(), HttpStatus.CREATED);
		 } catch (Exception e) {
		      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		 }
	}
	
	
	@GetMapping("/devices")
	  public ResponseEntity<List<DeviceApi>> getAllCompanyDevices(@PathVariable("companykey") String companyName) {
	
		List<DeviceApi> response = deviceService.getAllCompanyDevices(companyName);
	  return ResponseEntity.ok(response);
	}
	
	
	  @GetMapping("devices/device/{id}")
	  public ResponseEntity<?> getCompanyDeviceById(@PathVariable("companykey") String companyName, @PathVariable String deviceId) {

      Optional<DeviceApi> response = deviceService.getCompanyDeviceByDeviceId(companyName, deviceId);
	  
	  if( !response.isPresent())
	  {
		  return ResponseEntity
				  .status(HttpStatus.NOT_FOUND)
				  .body(new EmptyJsonObjectPayload()); 
	  }	 
		  
	  return ResponseEntity.ok(response.get());
	
	}
	  
	  
	  @PutMapping("devices/device/{id}")
	  public ResponseEntity<?> updateCompanyDeviceDetail(@PathVariable("companykey") String companyName, @PathVariable String deviceId, @Validated @RequestBody DeviceUpdateRequest deviceUpdateRequest) {

      Optional<DeviceApi> response = deviceService.updateCompanyDevice(companyName, deviceId, deviceUpdateRequest);
      
	  
	  if( !response.isPresent())
	  {
		  return ResponseEntity
				  .status(HttpStatus.NOT_FOUND)
				  .body(new EmptyJsonObjectPayload()); 
	  }	 
		  
	  return ResponseEntity.ok(response.get());
	
	}
		  
	  
	  @GetMapping("devices/device/{deviceId}")
	  public ResponseEntity<?> getDeviceByDeviceId(@PathVariable("companyname") String companyName, @PathVariable String deviceId) {
	
		  Optional<DeviceApi> response = deviceService.getCompanyDeviceByDeviceId(companyName, deviceId);
		  
		  if(!response.isPresent()) {
			  
			  return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new EmptyJsonObjectPayload());
		  }
	      return ResponseEntity.ok(response);
	
	}
	
	
//	  @GetMapping("device/{deviceOwner}")
//	  public ResponseEntity<List<Device>> getDeviceByOwner(@PathVariable String deviceOwner) {
//	  List<Device> response = deviceService.getDevicesByDeviceId(deviceOwner);
//	  return ResponseEntity.ok(response);
//	
//	}
//	
//   
//	  @GetMapping("device/{deviceStatus}")
//	  public ResponseEntity<List<Device>> getDeviceByStatus(@PathVariable String deviceStatus) {
//	  List<Device> response = deviceService.getDevicesByDeviceId(deviceStatus);
//	  return ResponseEntity.ok(response);
//	
//	}
	  
	  
}
