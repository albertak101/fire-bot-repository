package com.albert.finalYearProject.payload;

import java.time.Instant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class DeviceSensorReading {
	
	private Long id;	
	private Float sensorReading;
	private Instant readingDateTime;

}
