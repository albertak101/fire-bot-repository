package com.albert.finalYearProject.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmailAvailabilityResponse {
	
	private Boolean emailAvailability;

	

}
