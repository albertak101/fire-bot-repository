package com.albert.finalYearProject.payload;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class AddDeviceResponse {

	@JsonProperty("deviceId")	
	private String deviceId;

	@JsonProperty("deviceName")	
	private String deviceName;

	@JsonProperty("deviceKey")	
	private String deviceKey;


	 
}
