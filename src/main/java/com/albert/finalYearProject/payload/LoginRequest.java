package com.albert.finalYearProject.payload;



import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginRequest {
	
	@JsonProperty("username")	
    private String userNameOrEmail;
 
    private String password;    
    
    private Boolean rememberMe;
        
}
