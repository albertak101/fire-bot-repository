package com.albert.finalYearProject.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder

public class DeviceDataLoggerResponse {
	
	private String dataUploadStatus;
	private String dataUploadMessage;	
	

}
