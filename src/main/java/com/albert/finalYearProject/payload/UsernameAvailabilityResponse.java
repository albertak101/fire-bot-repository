package com.albert.finalYearProject.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsernameAvailabilityResponse {
	
	private Boolean usernameAvailability;


}
