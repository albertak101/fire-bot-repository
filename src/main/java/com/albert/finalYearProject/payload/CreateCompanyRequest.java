package com.albert.finalYearProject.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class CreateCompanyRequest {

	private String companyName;
	private String comment;
	private String contactEmailAddress;	
	private String contactTelephone;
	private String contactPersonName;
	
	
}
