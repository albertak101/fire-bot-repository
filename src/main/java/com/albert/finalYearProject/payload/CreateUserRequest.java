package com.albert.finalYearProject.payload;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class CreateUserRequest {
	
	@JsonProperty("firstName")	
	private String firstname;

	@JsonProperty("lastName")	
	private String surname;
	
	@JsonProperty("otherNames")	
	private String middlename;
	
	@JsonProperty( "dateOfBirth")	
	private String dateOfBirth;
	
	@JsonProperty( "email")	
	private String email;
	
	@JsonProperty( "phone")	
	private String phone;
	
	@JsonProperty( "userName")	
	private String username;
	
	@JsonProperty("password")	
	private String password;

}
