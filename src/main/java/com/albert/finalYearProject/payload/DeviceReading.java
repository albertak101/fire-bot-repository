package com.albert.finalYearProject.payload;

import java.util.List;

import com.albert.finalYearProject.core.SensorReadingTypes;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder

public class DeviceReading {
	
	private SensorReadingTypes sensorReadingType;
	private String deviceName;
	private String companyName;
	private float longitude;	
	private float latitude;
	
	private List<DeviceSensorReading> sensorReadings;

}
