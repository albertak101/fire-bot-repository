package com.albert.finalYearProject.payload;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class DeviceDataLoggerRequest {

	@JsonProperty("deviceId")	
	private String deviceId;

	@JsonProperty("humidity")	
	private Float humidity;

	@JsonProperty("temperature")	
	private Float temperature;
	
	@JsonProperty("pressure")	
	private Float pressure;

	@JsonProperty("altitude")	
	private Float altitude;

	@JsonProperty("co2")	
	private Float co2;
	
	@JsonProperty("readingTime")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Instant readingTime;

	
}
