package com.albert.finalYearProject.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class DeviceUpdateRequest {
	
   private String deviceName;	

	private String description;
	
	private Float latitude;
	
	private Float longitude;

}
