package com.albert.finalYearProject.payload;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class AddDeviceRequest {
	
	@JsonProperty("deviceName")	
	private String deviceName;

	@JsonProperty("companyId")	
	private String deviceOwnerId;

	@JsonProperty( "latitude")	
	private Float latitude;

	@JsonProperty("longitude")	
	private Float longitude;
	
	@JsonProperty("description")	
	private String description;
}
