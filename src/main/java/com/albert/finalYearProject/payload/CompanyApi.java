package com.albert.finalYearProject.payload;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class CompanyApi {
	
	@JsonIgnore
	private Long id;	

	private String companyName;	
	
	private String comment;		
	
	private String companyAccountNumber;

	@JsonProperty("companyId")
	private String companyKey;	

	private String contactEmailAddress;	
	
	private String contactTelephone;
	
	private String contactPersonName;

}
