package com.albert.finalYearProject.payload;

import java.time.Instant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder

public class DeviceApi {
	
	
	private Long id;		

	private String deviceId;	
	
	private String deviceKey;
	
	private String deviceName;	
	
	private Boolean isDeviceEnabled;
		
	private String description;
	
	private String companyId;
	
	private Float latitude;
	
	private Float longitude;	
		
	private Boolean isActivated;	
		
	private Instant dateTimeActivated;
		
	private String activationKey;
	


}
