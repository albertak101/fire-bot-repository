package com.albert.finalYearProject.payload;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;



@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder

public class UserProfile {
	
	private Long id;

	@JsonProperty("firstname")	
	private String firstname;

	@JsonProperty("lastname")	
	private String surname;
	
	@JsonProperty("othernames")	
	private String middlename;
	
	@JsonProperty( "dateofbirth")	
	private String dateOfBirth;
	
	@JsonProperty( "email")	
	private String email;
	
	@JsonProperty( "phone")	
	private String phone;
	
	@JsonProperty( "username")	
	private String username;
	
	private String activationKey;
	
	private String accountIdentity;
	
	private String companyId;
	
	@JsonIgnore
	private String password;
}
