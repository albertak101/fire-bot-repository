package com.albert.finalYearProject.payload;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder


public class CreateUserResponse {

	@JsonProperty("firstName")	
	private String firstName;

	@JsonProperty("lastName")	
	private String lastName;
	
	@JsonProperty( "email")	
	private String emailAddress;
	
	@JsonProperty( "userName")	
	private String userName;
	
	@JsonProperty( "dateOfBirth")	
	private String dateOfBirth;
	
	@JsonProperty("otherNames")	
	private String otherNames;
	
	@JsonProperty( "phone")	
	private String phone;
	
	
	
}
