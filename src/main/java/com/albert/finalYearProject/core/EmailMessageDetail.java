package com.albert.finalYearProject.core;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder

public class EmailMessageDetail {
	
	private String fromAddress;
	private String toAddress;
	private String subject;
	private String firstName;
	private String lastName;
	private String userName;
	//private String templateFileName;
}
