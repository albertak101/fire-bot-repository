package com.albert.finalYearProject.core;

public enum SensorReadingTypes {
   Temperature,
   Humidity,
   Pressure,
   Altitude,
   CarbonDioxide
}
