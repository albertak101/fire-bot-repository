package com.albert.finalYearProject.integration.email.services.Impl;

import java.io.UnsupportedEncodingException;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.mail.internet.InternetAddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.albert.finalYearProject.core.EmailMessageDetail;
import com.albert.finalYearProject.integration.email.services.EmailIntegrationServices;
import com.albert.finalYearProject.payload.AddDeviceResponse;
import com.albert.finalYearProject.payload.CompanyApi;
import com.albert.finalYearProject.payload.CreateUserResponse;
import com.google.common.collect.Lists;

import it.ozimov.springboot.mail.model.Email;
import it.ozimov.springboot.mail.model.defaultimpl.DefaultEmail;
import it.ozimov.springboot.mail.service.exception.CannotSendEmailException;


@Service
public class EmailIntegrationServicesImpl implements EmailIntegrationServices {
	
	  @Autowired
	   private it.ozimov.springboot.mail.service.EmailSchedulerService emailSchedulerService;

	  
	  
	  
	@Override
	public void dispatchDeviceRegistrationEmail(EmailMessageDetail emailMessageDetail, 	AddDeviceResponse deviceRegistrationResponse) {
		
		try {
	     	   final Email mimeEmail = DefaultEmail.builder()
		             .from(new InternetAddress(emailMessageDetail.getFromAddress(), "Fire Bot IOT"))
		             .to(Lists.newArrayList(new InternetAddress(emailMessageDetail.getToAddress(), emailMessageDetail.getFirstName() )))
		             .subject(emailMessageDetail.getSubject())
		             .body("")//Empty body
		             .encoding("UTF-8")
		             .build();
                  //Defining the model object for the given Freemarker template
                   final Map<String, Object> modelObject = new HashMap<>();
             
                  modelObject.put("request", deviceRegistrationResponse);
                  modelObject.put("contactPerson", emailMessageDetail.getFirstName());  
                  modelObject.put("companyName", emailMessageDetail.getUserName()); 
             
                  final OffsetDateTime scheduledDateTime = OffsetDateTime.now().plusSeconds(15);
                  final int priorityLevel = 1;                  
                  emailSchedulerService.schedule(mimeEmail, scheduledDateTime, priorityLevel,"mail/deviceRegistrationEmail.ftl", modelObject);
             
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (CannotSendEmailException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	}

	@Override
	public void dispatchTestEmailMessage(EmailMessageDetail emailMessageDetail, String msg) {
		try {
	     	final Email mimeEmail = DefaultEmail.builder()
		        .from(new InternetAddress(emailMessageDetail.getFromAddress(), "Fire Bot IOT"))
		        .to(Lists.newArrayList(new InternetAddress(emailMessageDetail.getToAddress(), emailMessageDetail.getFirstName() )))
		        .subject(emailMessageDetail.getSubject())
		        .body(msg)//Empty body
		        .encoding("UTF-8")
		        .build();
               
                final OffsetDateTime scheduledDateTime = OffsetDateTime.now().plusSeconds(15);
                final int priorityLevel = 1;                  
                emailSchedulerService.schedule(mimeEmail, scheduledDateTime, priorityLevel);
                
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 

	}

	@Override
	public void dispatchUserAccessAccountRegistrationEmail(EmailMessageDetail emailMessageDetail,  	CreateUserResponse userAccountApi) {
		
		try {
	     	   final Email mimeEmail = DefaultEmail.builder()
		             .from(new InternetAddress(emailMessageDetail.getFromAddress(), "Fire Bot IOT"))
		             .to(Lists.newArrayList(new InternetAddress(emailMessageDetail.getToAddress(), emailMessageDetail.getFirstName() )))
		             .subject(emailMessageDetail.getSubject())
		             .body("")//Empty body
		             .encoding("UTF-8")
		             .build();
                     //Defining the model object for the given Freemarker template
                      final Map<String, Object> modelObject = new HashMap<>();
                
                     modelObject.put("firstName", emailMessageDetail.getFirstName());
                     modelObject.put("user", userAccountApi);                     
                
                     final OffsetDateTime scheduledDateTime = OffsetDateTime.now().plusSeconds(15);
                     final int priorityLevel = 1;                  
                     emailSchedulerService.schedule(mimeEmail, scheduledDateTime, priorityLevel,"mail/registrationEmail.ftl", modelObject);
                
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (CannotSendEmailException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}

	@Override
	public void dispatchOtpEmail(EmailMessageDetail emailMessageDetail, String userName, String otpCode) {
		
		try {
	     	final Email mimeEmail = DefaultEmail.builder()
		        .from(new InternetAddress(emailMessageDetail.getFromAddress(), "Fire Bot IOT"))
		        .to(Lists.newArrayList(new InternetAddress(emailMessageDetail.getToAddress(), emailMessageDetail.getFirstName() )))
		        .subject(emailMessageDetail.getSubject())
		        .body("")//Empty body
		        .encoding("UTF-8")
		        .build();
                //Defining the model object for the given Freemarker template
                final Map<String, Object> modelObject = new HashMap<>();
                
                modelObject.put("firstName",  userName);
                modelObject.put("otpCode", otpCode);                     
                
                final OffsetDateTime scheduledDateTime = OffsetDateTime.now().plusSeconds(15);
                final int priorityLevel = 1;                  
                emailSchedulerService.schedule(mimeEmail, scheduledDateTime, priorityLevel,"mail/otpEmail.ftl", modelObject);
                
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (CannotSendEmailException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}

	@Override
	public void dispatchCompanyRegistrationEmail(EmailMessageDetail emailMessageDetail, CompanyApi companyApi) {
		
		try {
	     	   final Email mimeEmail = DefaultEmail.builder()
		             .from(new InternetAddress(emailMessageDetail.getFromAddress(), "Fire Bot IOT"))
		             .to(Lists.newArrayList(new InternetAddress(emailMessageDetail.getToAddress(), emailMessageDetail.getFirstName() )))
		             .subject(emailMessageDetail.getSubject())
		             .body("")//Empty body
		             .encoding("UTF-8")
		             .build();
                    //Defining the model object for the given Freemarker template
                   final Map<String, Object> modelObject = new HashMap<>();
             
                //  modelObject.put("firstName", emailMessageDetail.getFirstName());
                  modelObject.put("request", companyApi);                     
             
                  final OffsetDateTime scheduledDateTime = OffsetDateTime.now().plusSeconds(15);
                  final int priorityLevel = 1;                  
                  emailSchedulerService.schedule(mimeEmail, scheduledDateTime, priorityLevel,"mail/CompanyRegistrationEmail.ftl", modelObject);
             
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (CannotSendEmailException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
		
	}

	@Override
	public void dispatchUserAccountPasswordResetEmail(EmailMessageDetail emailMessageDetail, String newPassword) {
		try {
	     	final Email mimeEmail = DefaultEmail.builder()
		        .from(new InternetAddress(emailMessageDetail.getFromAddress(), "Fire Bot IOT"))
		        .to(Lists.newArrayList(new InternetAddress(emailMessageDetail.getToAddress(), emailMessageDetail.getFirstName() )))
		        .subject(emailMessageDetail.getSubject())
		        .body("")//Empty body
		        .encoding("UTF-8")
		        .build();
                //Defining the model object for the given Freemarker template
                final Map<String, Object> modelObject = new HashMap<>();
                
                modelObject.put("firstName",  emailMessageDetail.getFirstName());
                modelObject.put("userName", emailMessageDetail.getUserName()); 
                modelObject.put("defaultPassword", newPassword); 
                
                final OffsetDateTime scheduledDateTime = OffsetDateTime.now().plusSeconds(15);
                final int priorityLevel = 1;                  
                emailSchedulerService.schedule(mimeEmail, scheduledDateTime, priorityLevel,"mail/passwordResetEmail.ftl", modelObject);
                
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (CannotSendEmailException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}

	@Override
	public void dispatchAccountActivationEmail(EmailMessageDetail emailMessageDetail, String activationUrl) {
		
		try {
	     	final Email mimeEmail = DefaultEmail.builder()
		        .from(new InternetAddress(emailMessageDetail.getFromAddress(), "Fire Bot IOT"))
		        .to(Lists.newArrayList(new InternetAddress(emailMessageDetail.getToAddress(), emailMessageDetail.getFirstName() )))
		        .subject(emailMessageDetail.getSubject())
		        .body("")//Empty body
		        .encoding("UTF-8")
		        .build();
                //Defining the model object for the given Freemarker template
                final Map<String, Object> modelObject = new HashMap<>();
                
                modelObject.put("firstName",  emailMessageDetail.getFirstName());
                modelObject.put("userName", emailMessageDetail.getUserName()); 
                modelObject.put("activationUrl", activationUrl); 
                
                final OffsetDateTime scheduledDateTime = OffsetDateTime.now().plusSeconds(15);
                final int priorityLevel = 1;                  
                emailSchedulerService.schedule(mimeEmail, scheduledDateTime, priorityLevel,"mail/activationEmail.ftl", modelObject);
                
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (CannotSendEmailException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}

	@Override
	public void dispatchAccountWelcomeEmail(EmailMessageDetail emailMessageDetail, 	String loginUrl) {
		
		try {
	     	final Email mimeEmail = DefaultEmail.builder()
		        .from(new InternetAddress(emailMessageDetail.getFromAddress(), "Fire Bot IOT"))
		        .to(Lists.newArrayList(new InternetAddress(emailMessageDetail.getToAddress(), emailMessageDetail.getFirstName() )))
		        .subject(emailMessageDetail.getSubject())
		        .body("")//Empty body
		        .encoding("UTF-8")
		        .build();
                //Defining the model object for the given Freemarker template
                final Map<String, Object> modelObject = new HashMap<>();
                
                modelObject.put("firstName",  emailMessageDetail.getFirstName());
                modelObject.put("userName", emailMessageDetail.getUserName()); 
                modelObject.put("loginUrl", loginUrl); 
                
                final OffsetDateTime scheduledDateTime = OffsetDateTime.now().plusSeconds(15);
                final int priorityLevel = 1;                  
                emailSchedulerService.schedule(mimeEmail, scheduledDateTime, priorityLevel,"mail/welcomeEmail.ftl", modelObject);
                
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (CannotSendEmailException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}

	@Override
	public void dispatchAccountDeactivatedEmailMessage(EmailMessageDetail emailMessageDetail) {
		
		
		try {
	     	   final Email mimeEmail = DefaultEmail.builder()
		             .from(new InternetAddress(emailMessageDetail.getFromAddress(), "Fire Bot IOT"))
		             .to(Lists.newArrayList(new InternetAddress(emailMessageDetail.getToAddress(), emailMessageDetail.getFirstName() )))
		             .subject(emailMessageDetail.getSubject())
		             .body("")//Empty body
		             .encoding("UTF-8")
		             .build();
                  //Defining the model object for the given Freemarker template
                   final Map<String, Object> modelObject = new HashMap<>();
             
                  modelObject.put("firstName", emailMessageDetail.getFirstName());
                //  modelObject.put("user", userAccountApi);                     
             
                  final OffsetDateTime scheduledDateTime = OffsetDateTime.now().plusSeconds(15);
                  final int priorityLevel = 1;                  
                  emailSchedulerService.schedule(mimeEmail, scheduledDateTime, priorityLevel,"mail/registrationEmail.ftl", modelObject);
             
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (CannotSendEmailException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}

	@Override
	public void dispatchAccountDisabledEmailMessage(EmailMessageDetail emailMessageDetail) {
		
		try {
	     	   final Email mimeEmail = DefaultEmail.builder()
		             .from(new InternetAddress(emailMessageDetail.getFromAddress(), "Fire Bot IOT"))
		             .to(Lists.newArrayList(new InternetAddress(emailMessageDetail.getToAddress(), emailMessageDetail.getFirstName() )))
		             .subject(emailMessageDetail.getSubject())
		             .body("")//Empty body
		             .encoding("UTF-8")
		             .build();
                  //Defining the model object for the given Freemarker template
                   final Map<String, Object> modelObject = new HashMap<>();
             
                  modelObject.put("firstName", emailMessageDetail.getFirstName());
                  //modelObject.put("user", userAccountApi);                     
             
                  final OffsetDateTime scheduledDateTime = OffsetDateTime.now().plusSeconds(15);
                  final int priorityLevel = 1;                  
                  emailSchedulerService.schedule(mimeEmail, scheduledDateTime, priorityLevel,"mail/registrationEmail.ftl", modelObject);
             
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (CannotSendEmailException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
		
	}

	@Override
	public void dispatchDeviceActivationEmail(EmailMessageDetail emailMessageDetail, String deviceId , String activationUrl) {
		
		try {
	     	final Email mimeEmail = DefaultEmail.builder()
		        .from(new InternetAddress(emailMessageDetail.getFromAddress(), "Fire Bot IOT"))
		        .to(Lists.newArrayList(new InternetAddress(emailMessageDetail.getToAddress(), emailMessageDetail.getFirstName() )))
		        .subject(emailMessageDetail.getSubject())
		        .body("")//Empty body
		        .encoding("UTF-8")
		        .build();
                //Defining the model object for the given Freemarker template
                final Map<String, Object> modelObject = new HashMap<>();
                
                modelObject.put("firstName",  emailMessageDetail.getFirstName());
                modelObject.put("deviceId", deviceId); 
                modelObject.put("activationUrl", activationUrl); 
                
                final OffsetDateTime scheduledDateTime = OffsetDateTime.now().plusSeconds(15);
                final int priorityLevel = 1;                  
                emailSchedulerService.schedule(mimeEmail, scheduledDateTime, priorityLevel,"mail/deviceActivationEmail.ftl", modelObject);
                
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (CannotSendEmailException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}

}
