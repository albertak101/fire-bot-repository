package com.albert.finalYearProject.integration.email.services;

import com.albert.finalYearProject.core.EmailMessageDetail;
import com.albert.finalYearProject.payload.AddDeviceResponse;
import com.albert.finalYearProject.payload.CompanyApi;
import com.albert.finalYearProject.payload.CreateUserResponse;


public interface EmailIntegrationServices {
	
    void dispatchAccountActivationEmail(EmailMessageDetail emailMessageDetail, String activationUrl);
    
    void dispatchOtpEmail(EmailMessageDetail emailMessageDetail, String userName, String otpCode);
    
    void dispatchDeviceRegistrationEmail(EmailMessageDetail emailMessageDetail, AddDeviceResponse deviceRegistrationResponse);
    
    void dispatchTestEmailMessage(EmailMessageDetail emailMessageDetail, String msg);
	
	void dispatchAccountWelcomeEmail(EmailMessageDetail emailMessageDetail, String loginUrl);
	
	void dispatchUserAccessAccountRegistrationEmail(EmailMessageDetail emailMessageDetail, CreateUserResponse userAccountApi);
	
	void dispatchCompanyRegistrationEmail(EmailMessageDetail emailMessageDetail, CompanyApi companyApi);
	
	void dispatchUserAccountPasswordResetEmail(EmailMessageDetail emailMessageDetail, String newPassword);
	
	void dispatchAccountDeactivatedEmailMessage(EmailMessageDetail emailMessageDetail);
	
	void dispatchAccountDisabledEmailMessage(EmailMessageDetail emailMessageDetail);
	
	void dispatchDeviceActivationEmail(EmailMessageDetail emailMessageDetail,String deviceId, String activationUrl);
	

}
