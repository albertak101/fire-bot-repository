package com.albert.finalYearProject.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import it.ozimov.springboot.mail.configuration.EnableEmailTools;

@Configuration
@EnableEmailTools

public class EmailConfig {
	
	@Value("${spring.mail.host}")
	private String smtpHost;
	
	@Value("${spring.mail.port}")
	private Integer smtpPort;
	
	@Value("${spring.mail.username}")
	private String smtpUserName;
	
	@Value("${spring.mail.password}")
	private String smtpPassword;
	
	//@Value("${spring.mail}")
	//private Boolean enableDebugging;
	
	@Value("${spring.mail.test-connection}")
	private Boolean enableTestConnection;
	
	@Value("${spring.mail.protocol}")
	private String protocol;
	
	@Value("${spring.mail.from}")
	private String senderEmail;
	
	

}
